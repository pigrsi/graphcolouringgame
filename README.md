Graph Colouring Games

Running the program:
There are two methods to run the program.

1) Executable (RECOMMENDED): To run the program, simply run the executable file for Windows or Linux, depending on which operating system you are using. These are located in the dist folders. The Linux version was tested and will work in Ubuntu 14, and the Windows versions will work in the version stated on the folder name. It is possible they will run on other operating systems, but it is not guaranteed. You do not need to have Python installed.

2) Alternatively, run with Python: This program runs in Python 3. To run from you will need to install Python packages python-igraph and pygame. The method to do this depends on which operating system you are using. Instructions can be found on their respective websites. Once you have:
- Python 3
- igraph
- pygame
Make the src folder your current directory, and run the command "python3 main.py". The name for your Python 3 file may differ on your system.

This program will contain two different games:

1. The player is given a graph made up of vertices and edges. The
minimum number of colours required to solve the puzzle will be pre -
calculated by the program as best it can. The player’s goal is to colour
in each vertex, one by one, making sure each vertex has a different
colour than its neighbours. If they colour all the vertices using only the
minimum number of colours, they win the game. There will be many
different graphs for the player to choose from.

2. This game is similar to the single -player puzzle, but there will be an
adversary that tries to prevent you from solving the graph. The adversary
will be either a second player, or the computer. The two players take
turns colouring in vertices. The goal of the first player is the same as in
the first game. The goal of the second player is to prevent the first
player from accomplishing their goal. Specifically, t he second player
wins if the graph enters a state in which there exists a vertex that cannot
be coloured in a different colour to all its adjacent vertices (using the
minimum number of colours required to solve this graph). This is
known as a maker-breaker type of game.