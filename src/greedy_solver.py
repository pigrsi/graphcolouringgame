import igraph
import random
import math
import utility
from copy import deepcopy

# Greedy solver
# Tries to solve the graph
def greedy_method(graph):
    colors_available = set()
    color_list = utility.get_random_distinct_color(getList=True)
    color_index = 0

    i = 100
    random.seed()

    # Make a list of vertices from the graph's VertexSeq
    vertices_nocolor = []
    for v in graph.vs:
        vertices_nocolor.append(v)

    random.shuffle(vertices_nocolor)
    # Highest degrees first
    vertices_nocolor.sort(key=utility.degree_sort_key, reverse=True)

    # Try to solve
    while (len(vertices_nocolor) > 0):
        # Select vertex with highest vertex and remove it from uncolored list
        next_vertex = vertices_nocolor.pop(0)
        # Select the next color in a greedy way
        colors_allowed = colors_available
        colors_disallowed = set()
        for v_index in graph.neighbors(next_vertex):
            for color in colors_allowed:
                if color == graph.vs[v_index]["color"]:
                    colors_disallowed.add(color)
        colors_allowed = colors_allowed - colors_disallowed
        colors_allowed = list(colors_allowed)
        random.shuffle(colors_allowed)
        if len(colors_allowed) > 0:
            next_vertex["color"] = colors_allowed.pop(0)
        else:
            # Create new color
            new_color = color_list[color_index]
            color_index += 1
            colors_available.add(new_color)
            next_vertex["color"] = new_color

    return graph

# Run greedy method. Return list of all vertex colours in best result
def start_greedy(graph, numAttempts=1):
    minColors = -1
    minColorsList = None
    for i in range(0, numAttempts):
        greedy_method(graph)
        numColors = len(set(graph.vs["color"]))
        if minColors < 0 or numColors < minColors:
            minColors = numColors
            minColorList = graph.vs["color"]
        # Restore graph colors
        for v in graph.vs:
            v["color"] = v["color_prev"]
    return minColorList
