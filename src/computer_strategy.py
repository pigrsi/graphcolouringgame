from random import randint, sample, shuffle
from utility import *

def computer_select_vertex(gdInfo, graph, strategyMode=0):
    # If aggressive move is successful, Breaker will win this turn
    # If springtrap move is successful, Breaker will win in 2 turns
    # If laytrap move is successful, Breaker *may* win if Maker doesn't see the trap
    if strategyMode >= 1:
        Result = computer_select_aggressive(gdInfo, graph, strategyMode==2)
        if Result != False:
            return Result
    if strategyMode == 2:
        Result = computer_select_springtrap(gdInfo, graph)
        if Result != False:
            return Result
        Result = computer_select_laytrap(gdInfo, graph)
        if Result != False:
            return Result
        Result = computer_select_aggressive(gdInfo, graph, False)
        if Result != False:
            return Result
    return computer_select_random(gdInfo, graph)

def computer_select_springtrap(gdInfo, graph):
    # Make list of nodes with two colours left
    twoColorNodes = []
    for v in graph.vs:
        if len(get_allowed_colors_for_vertex(gdInfo.colors, graph, v)) == 2:
            twoColorNodes.append(v)
    shuffle(twoColorNodes)
    
    # Create list of all pairs of twocolor vertices
    twoColorPairs = []
    for i in range(0, len(twoColorNodes)):
        for k in range(i+1, len(twoColorNodes)):
            if k >= len(twoColorNodes):
                break
            else:
                twoColorPairs.append((twoColorNodes[i], twoColorNodes[k]))

    # Find a mutual neighbour between the two traps, that can be coloured in a colour usable by both traps.
    for pair in twoColorPairs:
        trap1 = pair[0]
        trap2 = pair[1]
        mutualVertices = get_mutual_vertices(trap1.neighbors(), trap2.neighbors())
        colors = list(get_mutual_usable_colors(trap1, trap2, graph, gdInfo.colors))
        shuffle(colors)
        # Find neighbours that can be coloured a colour that the traps could have used
        for v in mutualVertices:
            for c in colors:
                if c in get_allowed_colors_for_vertex(gdInfo.colors, graph, v):
                    return (v, c)

    return False

def computer_select_laytrap(gdInfo, graph):
    # Get list of nodes with 2 colours left
    twoColorNodes = []
    threeColorNodes = []
    for v in graph.vs:
        numColorsLeft = len(get_allowed_colors_for_vertex(gdInfo.colors, graph, v))
        if numColorsLeft == 2:
            twoColorNodes.append(v)
        elif numColorsLeft == 3:
            threeColorNodes.append(v)
    shuffle(twoColorNodes)
    shuffle(threeColorNodes)

    for trap1 in twoColorNodes:
        # Find a neighbour two nodes away with three colours left
        for neighbor in trap1.neighbors():
            for trap2 in neighbor.neighbors():
                if trap1 == trap2:
                    continue
                if trap2 in threeColorNodes:
                    # Colour a node *next to* trap2, that is not adjacent to trap1
                    trap2Friends = vertices_to_list(trap2.neighbors())
                    shuffle(trap2Friends)
                    for trapAdjacent in trap2Friends:
                        if trapAdjacent not in trap1.neighbors():
                            colors = list(get_allowed_colors_for_vertex(gdInfo.colors, graph, trapAdjacent))
                            if len(colors) > 0:
                                shuffle(colors)
                                for c in colors:
                                    trapAdjacentColor = c
                                    if trap_is_not_blocked(graph, gdInfo.colors, trap1, trap2, trapAdjacent, trapAdjacentColor):
                                        return (trapAdjacent, trapAdjacentColor)
    return False

# Make sure there is a vertex that can actually be coloured to trigger the trap. There needs to be a mutual neighbour between the two traps, that can be coloured in a colour usable by both traps. Also exclude the colour for trapAdjacent too as trapAdjacent is adjacent to Trap2. If there is not, go back to step 2.
# If there is at least one vertex that can be used to trigger the trap, assuming Maker does not interrupt
def trap_is_not_blocked(graph, palette, trap1, trap2, trapAdjacent, trapAdjacentColor):
    mutualVertices = get_mutual_vertices(trap1.neighbors(), trap2.neighbors())
    palette = palette.copy()
    mutualColors = get_mutual_usable_colors(trap1, trap2, graph, palette)
    if trapAdjacentColor in mutualColors:
        mutualColors.remove(trapAdjacentColor)
    for v in mutualVertices:
        for c in mutualColors:
            if c in get_allowed_colors_for_vertex(palette, graph, v):
                return True
    return False

# Try to go for a win
# If onlyKill, will not act aggressive unless it will win the game
def computer_select_aggressive(gdInfo, graph, onlyKill=False): 
    # Get vertex list in random order
    vs = vertex_seq_to_list(graph.vs)
    shuffle(vs)
    vs.sort(key=number_of_colors_sort_key, reverse=True)

    for v in vs:
        if v["color"] != v["uncolored_color"]:
            continue
        remainingColors = get_allowed_colors_for_vertex(gdInfo.colors, graph, v)
        if onlyKill and len(remainingColors) != 1:
            continue

        neighbors = vertex_seq_to_list(v.neighbors())
        shuffle(neighbors)
        for n in neighbors:
            nColors = list(get_allowed_colors_for_vertex(gdInfo.colors, graph, n))
            shuffle(nColors)
            for c in nColors:
                if c in remainingColors:
                    return (n, c)
    return False

def computer_select_random(gdInfo, graph):
    pairs = []
    for v in graph.vs:
        if v["color"] == v["uncolored_color"]:
            colors = get_allowed_colors_for_vertex(gdInfo.colors, graph, v)
            if len(colors) > 0:
                pairs.append((v, sample(colors, 1)[0]))
    if len(pairs) > 0:
        return pairs[randint(0, len(pairs)-1)]
    return None

# Favour vertices with more than one choice, otherwise the turn would be wasted
# The Kill: If a vertex only has one choice, see if you can make a neighbor that colour, if possible, you win the game
# Order uncoloured vertices by how many possible colours it can have

# Random:
# - Select a random vertex and give it a random legal colour.

# Aggressive (make a vertex run out of possible colours):
# - Randomize the order of the vertex list.
# - Find the vertex with the lowest possible number of colours left.
# - If there are multiple with the same number, pick the first one.
# - Store this set of remaining colours.
# - (Smarter aggressive starts here)
# - Loop through the neighbours
# - If you find a vertex that can be coloured as one of the remaining colours
#   of the initial vertex, assign it that colour.

# Smarter Aggressive (aggressive but with some extra checks): 
# - Order this vertex's neighbours, from most->least colours remaining.
# - Loop through the neighbours in that order.
