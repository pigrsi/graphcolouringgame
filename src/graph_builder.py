import pygame, collections, igraph
import utility

Position = collections.namedtuple('Position', 'x y')

class GraphBuilderInfo:
    def __init__(self, boundingBox, graph=None):
        self.gbuInfo = None
        self.boundingBox = boundingBox
        self.camera = Camera()
        self.backgroundColor = (238,200,200)
        self.gridColor = (170, 130, 160)
        self.gridSpacing = 16
        self.vertexRadius = 12
        self.potentialVertexPos = None
        self.builderMode = 0 # 0 - idle, 1 - vertex mode, 2 - edge mode
        self.selectedVertex = None
        self.draggingVertex = None
        self.graph = graph
        if graph == None:
            self.graph = igraph.Graph()
        self.graph["target"] = 0

    def update(self):
        self.camera.update()

    def add_vertex(self, position):
        for v in self.graph.vs:
            if utility.circles_overlap(position, self.vertexRadius+16, (v["x"], v["y"]), self.vertexRadius):
                return False
        self.graph.add_vertex()
        v = self.graph.vs[len(self.graph.vs)-1]
        v["x"] = round(position[0], 0)
        v["y"] = round(position[1], 0)
        self.gbuInfo.recalculate_target()
        return True

    def add_edge(self, vertex1, vertex2):
        if vertex2 in vertex1.neighbors():
            return False
        self.graph.add_edge(vertex1, vertex2)
        self.gbuInfo.recalculate_target()
        return True

    def clear_graph(self):
        self.graph = igraph.Graph()
        self.camera.set_position(0, 0)
        self.gbuInfo.graphTarget = 0

# Draws stuff offset by camera position
class Camera:
    def __init__(self):
        self.position = Position(x=0, y=0)
        self.speed = (0, 0)
        self.beingDragged = False

    def update(self):
        if self.beingDragged:
            self.beingDragged = False
        elif self.speed[0] != 0 or self.speed[1] != 0:
            self.speed = (self.speed[0]/1.125, self.speed[1]/1.125)
            self.translate(self.speed[0], self.speed[1], True)

    # Adjust position to take camera into account
    def adjust_pos(self, pos):
        return [pos[0]+self.position.x, pos[1]+self.position.y]

    def draw_circle(self, screen, color, pos, radius, width=0):
        pygame.draw.circle(screen, color, (int(pos[0]-self.position.x), int(pos[1]-self.position.y)), radius, width)

    def gfxdraw_filled_circle(self, screen, x, y, rad, color):
        pygame.gfxdraw.filled_circle(screen, int(x-self.position.x), int(y-self.position.y), rad, color)

    def gfxdraw_aacircle(self, screen, x, y, rad, color):
        pygame.gfxdraw.aacircle(screen, int(x-self.position.x), int(y-self.position.y), rad, color)

    def draw_line(self, screen, color, start_pos, end_pos, width=1):
        start_pos = (int(start_pos[0]-self.position.x), int(start_pos[1]-self.position.y))
        end_pos = (int(end_pos[0]-self.position.x), int(end_pos[1]-self.position.y))
        pygame.draw.line(screen, color, start_pos, end_pos, width)

    def translate(self, dx, dy, useAcceleration=True):
        self.position = Position(x=self.position.x+dx, y=self.position.y+dy)
        self.beingDragged = True
        if useAcceleration:
            self.speed = (dx, dy)
        else:
            self.speed = (0, 0)
        return True

    def set_position(self, x_, y_):
        self.position = Position(x=x_, y=y_)

    def recentre_on_graph(self, graph, windowSize):
        self.speed = (0, 0)
        if len(graph.vs) == 0:
            self.set_position(0, 0)
            return
        acc = [0, 0]
        for v in graph.vs:
            acc[0] += v["x"]
            acc[1] += v["y"]
        acc[0] /= len(graph.vs)
        acc[1] /= len(graph.vs)
        self.set_position(acc[0]-(windowSize[0]/2), acc[1]-(windowSize[1]/2))

def update_graph_builder(gbInfo):
    gbInfo.update()
    if gbInfo.builderMode == 0:
        if gbInfo.draggingVertex != None:
            pos = nearest_grid_position(gbInfo)
            gbInfo.draggingVertex["x"] = pos[0]+gbInfo.camera.position.x
            gbInfo.draggingVertex["y"] = pos[1]+gbInfo.camera.position.y
    elif gbInfo.builderMode == 1:
        update_potential_vertex(gbInfo)

def draw_graph_builder(screen, gbInfo):
    screen.fill(gbInfo.backgroundColor)
    draw_grid(screen, gbInfo)
    if gbInfo.builderMode == 2:
        draw_potential_edge(screen, gbInfo)
    elif gbInfo.builderMode == 1:
        draw_potential_vertex(screen, gbInfo)
    draw_edges(screen, gbInfo.graph, gbInfo)
    draw_vertices(screen, gbInfo.graph, gbInfo)

def draw_edges(screen, graph, gbInfo):
    vertices = graph.vs
    for e in graph.es:
        a = e.tuple[0]
        b = e.tuple[1]
        a_pos = (int(vertices[a]["x"]), int(vertices[a]["y"]))
        b_pos = (int(vertices[b]["x"]), int(vertices[b]["y"]))
        gbInfo.camera.draw_line(screen, (0,0,0), a_pos, b_pos, 2)

def draw_vertices(screen, graph, gbInfo):
    for v in graph.vs:
        gbInfo.camera.gfxdraw_filled_circle(screen, v["x"], v["y"], gbInfo.vertexRadius, (255,255,255))
        gbInfo.camera.gfxdraw_aacircle(screen, v["x"], v["y"], gbInfo.vertexRadius, (0,0,0))

def draw_grid(screen, gbInfo):
    startX = int(-gbInfo.camera.position.x % gbInfo.gridSpacing)
    startY = int(-gbInfo.camera.position.y % gbInfo.gridSpacing)
    for x in range(startX, gbInfo.boundingBox.width, gbInfo.gridSpacing):
        pygame.draw.line(screen, gbInfo.gridColor, (x,0), (x,gbInfo.boundingBox.height), 1)
    for y in range(startY, gbInfo.boundingBox.height, gbInfo.gridSpacing):
        pygame.draw.line(screen, gbInfo.gridColor, (0,y), (gbInfo.boundingBox.width,y), 1)

def update_potential_vertex(gbInfo):
    gbInfo.potentialVertexPos = nearest_grid_position(gbInfo)

def nearest_grid_position(gbInfo):
    camPos = gbInfo.camera.position
    mousePos = pygame.mouse.get_pos()
    potPosition = [mousePos[0], mousePos[1]]
    xOffset = -gbInfo.camera.position.x % gbInfo.gridSpacing
    yOffset = -gbInfo.camera.position.y % gbInfo.gridSpacing
    potPosition[0] = int(xOffset + utility.round_to_nearest(potPosition[0], gbInfo.gridSpacing))
    potPosition[1] = int(yOffset + utility.round_to_nearest(potPosition[1], gbInfo.gridSpacing))
    return potPosition

def draw_potential_edge(screen, gbInfo):
    if gbInfo.selectedVertex is not None:
        v = gbInfo.selectedVertex
        mousePos = gbInfo.camera.adjust_pos(pygame.mouse.get_pos())
        gbInfo.camera.draw_line(screen, (0,0,0), (v["x"],v["y"]), mousePos, 1)

def draw_potential_vertex(screen, gbInfo):
    # Draw vertex on closest grid spot
    if gbInfo.potentialVertexPos == None:
        return
    potPosition = gbInfo.potentialVertexPos
    pygame.gfxdraw.filled_circle(screen, potPosition[0], potPosition[1], gbInfo.vertexRadius, (255,255,255))
    pygame.gfxdraw.aacircle(screen, potPosition[0], potPosition[1], gbInfo.vertexRadius, (0,0,0))

def place_new_vertex(gbInfo):
    pos = gbInfo.potentialVertexPos
    pos = (pos[0]+gbInfo.camera.position.x, pos[1]+gbInfo.camera.position.y)
    gbInfo.add_vertex(pos)

class GBUserInput:
    def __init__(self, gbInfo):
        self.IDLE = 0
        self.PANNING = 1
        self.state = self.IDLE
        self.gbInfo = gbInfo
        self.selectedVertex = None
        self.draggingVertex = None
        self.panningWithKeys = False
        self.lastPanningKey = None

    def update(self):
        relative = pygame.mouse.get_rel()
        if self.state == self.PANNING:
            self.gbInfo.camera.translate(-relative[0], -relative[1])
        
        # Check key panning
        if self.panningWithKeys:
            panned = False
            if pygame.key.get_pressed()[pygame.K_UP]: panned = self.gbInfo.camera.translate(0, -10, False)
            if pygame.key.get_pressed()[pygame.K_DOWN]: panned = self.gbInfo.camera.translate(0, 10, False)
            if pygame.key.get_pressed()[pygame.K_LEFT]: panned = self.gbInfo.camera.translate(-10, 0, False)
            if pygame.key.get_pressed()[pygame.K_RIGHT]: panned = self.gbInfo.camera.translate(10, 0, False)
            if not panned:
                self.panningWithKeys = False

    def mouse_down(self, mousePos, button):
        if not self.gbInfo.boundingBox.collidepoint(mousePos):
            return

        # Right click
        if button == 3:
            # Stop dragging new edge. If you're not, exit current builder mode
            if self.gbInfo.builderMode == 2 and self.gbInfo.selectedVertex != None:
                self.gbInfo.selectedVertex = None
            elif self.gbInfo.builderMode != 0:
                self.gbInfo.gbuInfo.set_builder_mode(0)
            else:
                # Don't allow deleting vertices when dragging one
                if self.gbInfo.draggingVertex == None:
                    for v in self.gbInfo.graph.vs:
                        if utility.point_is_within_vertex(self.gbInfo.camera.adjust_pos(mousePos), v, self.gbInfo.vertexRadius):
                            self.gbInfo.graph.delete_vertices(v)
                            self.gbInfo.gbuInfo.recalculate_target()
                            break
            return
        
        if self.state == self.IDLE:
            if self.gbInfo.builderMode == 0:
                vertexClicked = False
                if self.gbInfo.draggingVertex != None:
                    self.gbInfo.draggingVertex = None
                    vertexClicked = True
                if not vertexClicked:
                    for v in self.gbInfo.graph.vs:
                        if utility.point_is_within_vertex(self.gbInfo.camera.adjust_pos(mousePos), v, self.gbInfo.vertexRadius):
                            vertexClicked = True
                            self.draggingVertex = v
                            break
                if not vertexClicked:
                    self.state = self.PANNING
            elif self.gbInfo.builderMode == 1:
                place_new_vertex(self.gbInfo)
            elif self.gbInfo.builderMode == 2:
                for v in self.gbInfo.graph.vs:
                    if utility.point_is_within_vertex(self.gbInfo.camera.adjust_pos(mousePos), v, self.gbInfo.vertexRadius):
                            self.selectedVertex = v
                if self.selectedVertex == None:
                    self.state = self.PANNING

    def mouse_up(self, mousePos, button):
        if self.state == self.PANNING:
            self.state = self.IDLE
        if self.gbInfo.builderMode == 0:
            # Dragging
            if self.draggingVertex != None:
                if utility.point_is_within_vertex(self.gbInfo.camera.adjust_pos(mousePos), self.draggingVertex, self.gbInfo.vertexRadius):
                    self.gbInfo.draggingVertex = self.draggingVertex
                self.draggingVertex = None
        elif self.gbInfo.builderMode == 2:
            if self.selectedVertex != None:
                if utility.point_is_within_vertex(self.gbInfo.camera.adjust_pos(mousePos), self.selectedVertex, self.gbInfo.vertexRadius):
                    if self.gbInfo.selectedVertex == None:
                        self.gbInfo.selectedVertex = self.selectedVertex
                    elif self.gbInfo.selectedVertex != self.selectedVertex:
                        self.gbInfo.add_edge(self.gbInfo.selectedVertex, self.selectedVertex)
                        self.gbInfo.selectedVertex = None
        self.selectedVertex = None

    def key_down(self, key, gbInfo):
        if key == pygame.K_r:
            self.gbInfo.camera.recentre_on_graph(gbInfo.graph, (gbInfo.boundingBox.width, gbInfo.boundingBox.height))
        elif key == pygame.K_UP or key == pygame.K_DOWN or \
            key == pygame.K_LEFT or key == pygame.K_RIGHT:
            self.panningWithKeys = True
            self.lastPanningKey = key

