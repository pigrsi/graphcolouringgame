import pygame, igraph
import pygame.gfxdraw
import math, colorsys
from random import random
from ui_button import Button
import utility

# Store info about the UI
class PaletteBoxInfo:
    def __init__(self, bottomBarRect, innerColor, borderColor, borderWidth, colorTarget, fontName, graphDisplayInfo):
        self.gdInfo = graphDisplayInfo

        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 24)
        text1 = buttonFont.render("Help", True, (0,0,0))
        text2 = buttonFont.render("Reset graph", True, (0,0,0))
        text3 = buttonFont.render("Next graph", True, (0,0,0))
        text4 = buttonFont.render("Exit to main menu", True, (0,0,0))
        self.menuButtons = [Button(self.toggle_show_help, [], color=(255,255,255), textSurface=text1),
        Button(self.reset, [], color=(255,255,255), textSurface=text2),
        Button(self.gdInfo.return_to_menu, [True], color=(255,255,255), textSurface=text3),
        Button(self.gdInfo.return_to_menu, [], color=(255,255,255), textSurface=text4)]  
        self.showHelp = False
        if self.gdInfo.mbMode:
            self.helpImage = pygame.image.load("../assets/help_mb.png")
        else:
            self.helpImage = pygame.image.load("../assets/help_solo.png")
        self.helpImageClean = self.helpImage
        self.updateSizes(bottomBarRect)
        self.showMenu = False
        self.bottomBarYOffset = 0
        self.innerColor = innerColor
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.colorTarget = colorTarget = self.gdInfo.graph["target"]
        self.extraColors = 0
        self.font = pygame.font.Font(pygame.font.match_font(fontName), 30)
        self.fontSmall = pygame.font.Font(pygame.font.match_font(fontName), 24)
        self.paletteButtons = []
        self.splodgeImages = [pygame.image.load("../assets/splodge_white.png").convert_alpha(), 
        pygame.image.load("../assets/splodge_highlight.png").convert_alpha()]
        plusImage = pygame.image.load("../assets/plus_filled.png").convert_alpha()
        self.plusButton = Button(self.toggle_color_wheel_visiblity, [], plusImage)
        text0 = buttonFont.render("Menu", True, (0,0,0))
        self.menuButton = Button(self.toggle_show_menu, [], None, self.gdInfo.uncoloredColor, textSurface=text0)
        self.bottomBarHidden = False
        hideArrowImage = pygame.image.load("../assets/hidearrow.png").convert_alpha()
        self.hideArrowImages = []
        hideButtonSurface = pygame.Surface((hideArrowImage.get_width(), hideArrowImage.get_height()))
        hideButtonSurface.fill((90,255,90))
        hideButtonSurface.blit(hideArrowImage, (0,0))
        hideButtonSurface.set_alpha(128)
        self.hideArrowImages.append(hideButtonSurface)
        self.hideArrowImages.append(pygame.transform.flip(self.hideArrowImages[0], False, True))
        self.hideButton = Button(self.toggle_hide_bottom_bar, [], self.hideArrowImages[0])
        # For when palette doesn't fit
        self.paletteButtonsIndex = 0
        self.paletteScrollButtons = [Button(self.scroll_palette_buttons, [-1], fillColorImage=True, canPulsate=False), 
        Button(self.scroll_palette_buttons, [1], fillColorImage=True, canPulsate=False)]
        # Buttons for when maker breaker game ends
        buttonTexts = [buttonFont.render("Next graph", True, (0,0,0)), buttonFont.render("Play again", True, (0,0,0)),
        buttonFont.render("Play again with +1 colour", True, (0,0,0)), buttonFont.render("Back to main menu", True, (0,0,0))]
        buttonFunctions = [(self.gdInfo.return_to_menu, [True]), (self.reset, []), (self.reset, [True, 1]), (self.gdInfo.return_to_menu, [])]
        buttonPulsates = [False, False, False, True]
        self.endgameButtons = []
        rect = pygame.Rect(32, 32, buttonTexts[2].get_width()+16, 48)
        for i in range(0, 4):
            rect = rect.copy()
            rect.y += 16+rect.height
            startPos=(-rect.width*(1+i), rect.y)
            midPosition = ((startPos[0]+rect.x)/2, (startPos[1]+rect.y)/2)
            self.endgameButtons.append(Button(buttonFunctions[i][0], buttonFunctions[i][1], textSurface=buttonTexts[i], 
                                        startAtPos=startPos, posRect=rect, midPos=midPosition, dontGrow=True, canPulsate=buttonPulsates[i]))
        # A list for every type of button
        self.allButtons = list(self.paletteButtons)
        self.allButtons.extend([self.plusButton, self.menuButton, self.hideButton])
        self.allButtons.extend(self.paletteScrollButtons)
        self.reset(False)
        if self.gdInfo.mbMode:
            # If maker-breaker, create the palette before the game starts
            for i in range(0, self.colorTarget):
                colour = utility.get_random_distinct_color(self.gdInfo.colors)
                self.new_palette_button(colour)

    def reset(self, resetGraph=True, numExtraColors=0):
        if resetGraph:
            self.gdInfo.reset()
        self.usedColorAngles = []
        self.colorWheelCentreColor = None
        self.colorWheelVisible = False
        self.colorWheelPos = None
        self.colorWheelPrevAngle = -1
        self.colorWheelUsedSize = 52
        self.colorWheelCentreRadius = 38
        self.colorWheel = create_color_wheel(96, self)
        self.paletteButtonsIndex = 0
        if self.gdInfo.mbMode == 0:
            for button in self.paletteButtons:
                if button in self.allButtons:
                    self.allButtons.remove(button)
            self.paletteButtons = []
        self.extraColors += numExtraColors
        for i in range(0, numExtraColors):
            self.new_palette_button(utility.get_random_distinct_color(self.gdInfo.colors))

    def scroll_palette_buttons(self, increment):
        self.paletteButtonsIndex += increment
        if self.paletteButtonsIndex < 0:
            self.paletteButtonsIndex = 0
        elif self.paletteButtonsIndex >= len(self.paletteButtons):
            self.paletteButtonsIndex = len(self.paletteButtons)-1

    def toggle_show_help(self):
        self.showHelp = not self.showHelp

    def toggle_hide_bottom_bar(self):
        self.bottomBarHidden = not self.bottomBarHidden

    def toggle_color_wheel_visiblity(self):
        self.colorWheelVisible = not self.colorWheelVisible
        self.colorWheelCentreColor = None
        
    def toggle_show_menu(self):
        self.showMenu = not self.showMenu
        self.gdInfo.paused = self.showMenu
        self.showHelp = False

    def game_ended(self):
        for button in self.endgameButtons:
            button.reset_bezier()
        self.endgameButtons[2].greyed = (self.gdInfo.winnerPlayer == 0) or (len(self.paletteButtons)+1 > self.gdInfo.graph.maxdegree())

    def updateSizes(self, bottomBarRect):
        bbr = bottomBarRect
        self.bottomBarRect = bbr
        self.targetBoxRect = pygame.Rect(0, bbr.top, 156, bbr.height)
        self.menuButtonBoxRect = bottomBarRect.copy()
        self.menuButtonBoxRect = pygame.Rect(bbr.right-128, bbr.top, 128, bbr.height)
        self.paletteBoxRect = pygame.Rect(self.targetBoxRect.right, bbr.top, 
            bbr.right-self.menuButtonBoxRect.width-self.targetBoxRect.width, bbr.height)
        greyHeight = self.gdInfo.boundingBox.height + bottomBarRect.height
        self.greySurface = pygame.Surface((self.gdInfo.boundingBox.width, greyHeight), pygame.SRCALPHA)
        self.greySurface.fill((0,0,0,90)) 
        menuSize = (260, 280)
        buttonSize = (menuSize[0]-30, 48)
        padding = 16
        menuPos = (self.gdInfo.boundingBox.centerx - menuSize[0]/2, self.gdInfo.boundingBox.centery - menuSize[1]/2)
        self.menuRect = pygame.Rect(menuPos[0], menuPos[1], menuSize[0], menuSize[1])
        menuButtonsHeight = padding*(len(self.menuButtons)+2) + buttonSize[1]*len(self.menuButtons)
        prevBottom = 8+menuPos[1]+menuSize[1]/2-menuButtonsHeight/2
        for button in self.menuButtons:
            rect = pygame.Rect(menuPos[0]+menuSize[0]/2-buttonSize[0]/2, prevBottom+padding, buttonSize[0], buttonSize[1])
            button.posRect = rect
            prevBottom = rect.bottom
        box = self.gdInfo.boundingBox
        help = self.helpImageClean
        widthDecrease = 0
        if help.get_width() > box.width-64:
            widthDecrease = help.get_width() - box.width + 64
        size = (int(help.get_width()-widthDecrease), int(help.get_height()*((help.get_width()-widthDecrease)/help.get_width())))
        self.helpRect = pygame.Rect(box.centerx-size[0]/2, box.centery-size[1]/2, size[0], size[1])
        if widthDecrease > 0:
            self.helpImage = pygame.transform.smoothscale(self.helpImageClean, size)

    def new_palette_button(self, color):
        # Create image for color splodge
        image = self.splodgeImages[0].copy()
        image.fill(color, None, pygame.BLEND_RGBA_MULT)
        image.blit(self.splodgeImages[1], (0,0))
        image = pygame.transform.smoothscale(image, (96,96))
        if self.colorWheelPos is None:
            startPos = (self.bottomBarRect.x+self.bottomBarRect.width/2-48, -128)
        else:
            startX = self.colorWheelPos[0] + self.colorWheel.get_width()
            startY = self.colorWheelPos[1] + self.colorWheel.get_height()*1.2
            startPos = (startX, startY)
        newButton = Button(set_paint_to_button_color, [self, color], image, color, None, startPos)
        self.paletteButtons.append(newButton)
        self.allButtons.append(newButton)
        self.gdInfo.update_color_set(color)
        self.gdInfo.check_uncolorable_vertices()

    def hide_bottom_bar(self):
        if self.bottomBarYOffset < self.bottomBarRect.height-4:
            self.bottomBarRect.y += 4
            self.bottomBarYOffset += 4
            self.gdInfo.boundingBox.height += 4
            if self.bottomBarYOffset >= self.bottomBarRect.height-4:
                self.hideButton.set_image(self.hideArrowImages[1])
        self.updateSizes(self.bottomBarRect)

    def show_bottom_bar(self):
        if self.bottomBarYOffset > 0:
            self.bottomBarRect.y -= 4
            self.bottomBarYOffset -= 4
            self.gdInfo.boundingBox.height -= 4
            if self.bottomBarYOffset <= 0:
                self.hideButton.set_image(self.hideArrowImages[0])
        self.updateSizes(self.bottomBarRect)

# Button actions
def set_paint_to_button_color(pbInfo, color):
    pbInfo.gdInfo.set_paint_color(color)
def add_color_to_palette(pbInfo, color):
    pbInfo.new_palette_button(color)
    if pbInfo.colorWheelPrevAngle >= 0:
        pbInfo.usedColorAngles.append(pbInfo.colorWheelPrevAngle)
        pbInfo.colorWheel = create_color_wheel(96, pbInfo)
        set_paint_to_button_color(pbInfo, color)
        pbInfo.colorWheelPrevAngle = -1

def update_ui(pbInfo, delta):
    for button in pbInfo.allButtons:
        button.update(delta)
    if pbInfo.showMenu:
        for button in pbInfo.menuButtons:
            button.update(delta)
    if pbInfo.gdInfo.winnerPlayer != -1:
        for button in pbInfo.endgameButtons:
            button.update(delta)

# Find which color was clicked
def get_color_wheel_clicked(pbInfo, mousePos):
    if not pbInfo.colorWheelVisible or pbInfo.colorWheelPos == None:
        return False
    # Find colour in colour wheel
    wheelSurface = pbInfo.colorWheel
    wheelWidth = wheelSurface.get_width()
    wheelHeight = wheelSurface.get_height()
    radius = wheelWidth/2
    # x and y of mouse assuming the centre of the wheel is at (0,0)
    x = mousePos[0] - pbInfo.colorWheelPos[0] - radius
    y = mousePos[1] - pbInfo.colorWheelPos[1] - radius
    distance = math.sqrt(x*x + y*y)
    if (distance <= radius and distance >= pbInfo.colorWheelCentreRadius):
        # Get color at this position
        phi = math.degrees(math.atan2(y, x) + math.pi)
        # Make sure a color was not used to close to this
        if angle_allowed(phi, pbInfo.usedColorAngles, pbInfo.colorWheelUsedSize):
            colorTemp = colorsys.hsv_to_rgb(phi/360, distance/radius, 1)
            color = (int(colorTemp[0]*255), int(colorTemp[1]*255), int(colorTemp[2]*255))
            pbInfo.colorWheelPrevAngle = phi
            return color
        else:
            return (0,0,0)
    elif distance < pbInfo.colorWheelCentreRadius:
        return (255,255,255)
    return False

# True - the angle is allowed to be used
def angle_allowed(angle, prevAngles, chipSize, reverse=False):
    for a in prevAngles:
        prevA = (a-chipSize/2)
        prevB = (a+chipSize/2)
        if reverse:
            temp = prevA
            prevA = prevB
            prevB = temp
        mid = angle

        diff = prevB - prevA
        if diff < 0:
            prevB = prevB - prevA + 360
        else:
            prevB = prevB - prevA

        diff = mid - prevA
        if diff < 0:
            mid = mid - prevA + 360
        else:
            mid = mid - prevA
        
        if mid < prevB:
            return False
        elif not reverse:
            if angle_allowed(angle, [a], chipSize, True):
                return False
    return True 

def update_palette_buttons(pbInfo):
    pBox = pbInfo.paletteBoxRect   
    buttons = pbInfo.paletteButtons
    gapWidth = 8
    buttonWidth = 96
    if pbInfo.gdInfo.mbMode == 0:
        centre = pBox.centerx-buttonWidth/2
    else:
        centre = pBox.centerx
    # Find position of leftmost button
    gapSum = gapWidth * len(buttons)-1
    buttonWidthSum = buttonWidth * len(buttons)
    totalButtonsWidth = gapSum + buttonWidthSum
    start = centre - totalButtonsWidth/2
    buttonRect = pygame.Rect(start, pBox.top+14, buttonWidth, buttonWidth)

    # Check if buttons fit within the box
    if pbInfo.gdInfo.mbMode == 0:
        # Remove width for plus button
        plusWidth = buttonWidth
        middleWidth = pbInfo.menuButtonBoxRect.left - pbInfo.targetBoxRect.right - plusWidth - 64
    else:
        plusWidth = buttonWidth/2
        middleWidth = pbInfo.menuButtonBoxRect.left - pbInfo.targetBoxRect.right
    if buttonWidthSum > middleWidth:
        # Use palette button scroller
        for button in buttons:
            if button == pbInfo.paletteButtons[pbInfo.paletteButtonsIndex]:
                button.set_position(pygame.Rect(pBox.centerx-plusWidth, pBox.top+14, buttonWidth, buttonWidth))
            else:
                button.set_position(None)
        arrowWidth = 32
        h = 64
        pbInfo.paletteScrollButtons[0].set_position(pygame.Rect(centre-buttonWidth/2-arrowWidth-8, pBox.centery-32, arrowWidth, h))
        pbInfo.paletteScrollButtons[1].set_position(pygame.Rect(centre+buttonWidth/2+8, pBox.centery-32, arrowWidth, h))
        # Create images for scroll buttons
        if pbInfo.paletteScrollButtons[0].image == None:
            off = 8
            arrow = pygame.Surface((arrowWidth+2, h+2), pygame.SRCALPHA)
            w = arrowWidth
            pointList = [(0,0), (w, h/2), (0,h)]
            pygame.draw.polygon(arrow, (255,255,255), pointList)
            pygame.draw.polygon(arrow, (0,0,0), pointList, 2)
            pbInfo.paletteScrollButtons[1].set_image(arrow)
            arrow = pygame.transform.flip(arrow, True, False)
            pbInfo.paletteScrollButtons[0].set_image(arrow)
    else:
        for button in buttons:
            button.set_position(buttonRect)
            buttonRect = buttonRect.move(buttonWidth+gapWidth, 0)
        for button in pbInfo.paletteScrollButtons:
            button.set_position(None)

    buttonRect = buttonRect.copy()
    buttonRect.x = pBox.right-96-gapWidth
    buttonRect.y = pBox.top+16
    if pbInfo.gdInfo.mbMode == 0:
        pbInfo.plusButton.set_position(buttonRect)
    else:
        pbInfo.plusButton.set_position(None)

    if pbInfo.colorWheelVisible:
        pbInfo.colorWheelPos = [pbInfo.plusButton.posRect.centerx-pbInfo.colorWheel.get_width()/2,
                                pbInfo.bottomBarRect.y-pbInfo.colorWheel.get_height()-16]

def draw_palette_buttons(screen, pbInfo):
    for button in pbInfo.paletteButtons:
        button.draw(screen)
    # Set color of next/prev buttons
    if pbInfo.paletteScrollButtons[0].posRect != None:
        color = (255,255,255)
        if pbInfo.paletteButtonsIndex-1 >= 0:
            color = pbInfo.paletteButtons[pbInfo.paletteButtonsIndex-1].color
            pbInfo.paletteScrollButtons[0].color = color
            pbInfo.paletteScrollButtons[0].draw(screen)
        if pbInfo.paletteButtonsIndex+1 < len(pbInfo.paletteButtons):
            color = pbInfo.paletteButtons[pbInfo.paletteButtonsIndex+1].color
            pbInfo.paletteScrollButtons[1].color = color
            pbInfo.paletteScrollButtons[1].draw(screen)

    pbInfo.plusButton.draw(screen)
    
def update_menu_button(pbInfo):
    bb = pbInfo.menuButtonBoxRect
    bWidth = 96
    pbInfo.menuButton.set_position(pygame.Rect(bb.centerx-bWidth/2, bb.centery-bWidth/2, bWidth, bWidth))

def update_hide_button(pbInfo):
    bb = pbInfo.menuButtonBoxRect
    pbInfo.hideButton.set_position(pygame.Rect(bb.x+32, bb.y-64, 64, 64))

def draw_ui(screen, pbInfo):
    # Hiding palette box
    if pbInfo.bottomBarHidden:
        pbInfo.hide_bottom_bar()
    else:
        pbInfo.show_bottom_bar()

    # Update button positions
    # Positions probably don't need to be calculated repeatedly unless they're moving
    update_palette_buttons(pbInfo)
    update_menu_button(pbInfo)
    update_hide_button(pbInfo)

    # Draw palette box at the bottom
    # Draw background color for bottom border
    pygame.draw.rect(screen, pbInfo.innerColor, pbInfo.bottomBarRect, 0)
    draw_window_border(screen, pbInfo)
    draw_bottom_bar(screen, pbInfo)
    draw_target_text(screen, pbInfo)
    draw_palette_buttons(screen, pbInfo)
    pbInfo.menuButton.draw(screen)
    pbInfo.hideButton.draw(screen)
    if pbInfo.gdInfo.mbMode == 0 and pbInfo.gdInfo.allColored:
        draw_solo_text(screen, pbInfo)
    elif pbInfo.gdInfo.mbMode:
        draw_turn_text(screen, pbInfo)

    if pbInfo.gdInfo.winnerPlayer != -1:
        for button in pbInfo.endgameButtons:
            button.draw(screen)

    # Colour wheel
    if pbInfo.colorWheelVisible and pbInfo.colorWheelPos is not None:
        if pbInfo.colorWheelCentreColor is not None:
            rad = pbInfo.colorWheel.get_width()/2
            pygame.draw.circle(screen, pbInfo.colorWheelCentreColor, 
                              (int(pbInfo.colorWheelPos[0]+rad), int(pbInfo.colorWheelPos[1]+rad)), pbInfo.colorWheelCentreRadius)
        screen.blit(pbInfo.colorWheel, pbInfo.colorWheelPos)

    # Menu
    if pbInfo.showMenu:
        draw_menu_box(screen, pbInfo)
        if not pbInfo.showHelp:
            for button in pbInfo.menuButtons:
                button.draw(screen)

def draw_bottom_bar(screen, pbInfo):
    left = pbInfo.bottomBarRect.left
    right = pbInfo.bottomBarRect.right
    bottom = pbInfo.bottomBarRect.bottom
    top = pbInfo.bottomBarRect.top
    targetRight = pbInfo.targetBoxRect.right
    menuButtonLeft = pbInfo.menuButtonBoxRect.left
    # Top
    pygame.draw.line(screen, pbInfo.borderColor, (left, top), (right, top), pbInfo.borderWidth)
    # Bar separating Target
    pygame.draw.line(screen, pbInfo.borderColor, (targetRight, top), (targetRight, bottom), pbInfo.borderWidth)
    # Bar separating menu button
    pygame.draw.line(screen, pbInfo.borderColor, (menuButtonLeft, top), (menuButtonLeft, bottom), pbInfo.borderWidth)

def draw_window_border(screen, pbInfo):
    w2 = pbInfo.borderWidth/2
    right = pbInfo.bottomBarRect.right
    bottom = pbInfo.bottomBarRect.bottom
    pygame.draw.line(screen, pbInfo.borderColor, (0,0),(right,0), pbInfo.borderWidth)
    pygame.draw.line(screen, pbInfo.borderColor, (right-w2,0),(right-w2,bottom), pbInfo.borderWidth)
    pygame.draw.line(screen, pbInfo.borderColor, (right,bottom-w2),(0,bottom-w2), pbInfo.borderWidth)
    pygame.draw.line(screen, pbInfo.borderColor, (w2-1,bottom),(w2-1,0), pbInfo.borderWidth)

def draw_target_text(screen, pbInfo):
    textSurface = pbInfo.font.render("Target:", True, (255,0,0))
    exText = ""
    if pbInfo.extraColors > 0:
        exText = "+" + str(pbInfo.extraColors)
    textSurface2 = pbInfo.font.render(str(pbInfo.colorTarget)+exText, True, (255,0,0))
    x = pbInfo.targetBoxRect.x + pbInfo.targetBoxRect.width/2 - textSurface.get_width()/2
    y = pbInfo.targetBoxRect.y + pbInfo.targetBoxRect.height/2 - textSurface.get_height()/2 - 18
    screen.blit(textSurface, (x,y))
    x = x + textSurface.get_width()/2 - textSurface2.get_width()/2
    y = y+textSurface.get_height()+8
    screen.blit(textSurface2, (x,y))

def draw_help_box(screen, pbInfo):
    pygame.draw.rect(screen, (255,255,255), pbInfo.helpRect)
    pygame.draw.rect(screen, pbInfo.borderColor, pbInfo.helpRect, 8)
    screen.blit(pbInfo.helpImage, (pbInfo.helpRect.x, pbInfo.helpRect.y))

# Whose turn it is in the maker-breaker game
def draw_turn_text(screen, pbInfo):
    turnPlayer = pbInfo.gdInfo.turnPlayer
    color = (0, 0, 255)
    if pbInfo.gdInfo.winnerPlayer == 0:
        string = "Maker has done it! You failed, Breaker!"
        color = (0, 128, 0)
    elif pbInfo.gdInfo.winnerPlayer == 1:
        string = "Breaker triumphs! Maker, you've been outplayed!"
        color = (0, 128, 0)
    elif turnPlayer == 0:
        string = "Maker's turn"
    else:
        string = "Breaker's turn"
        color = (210, 0, 0)
    box = pbInfo.gdInfo.boundingBox
    textSurface = pbInfo.font.render(string, True, color)
    x = box.x + 16
    y = box.y + 16
    screen.blit(textSurface, (x,y))

def draw_solo_text(screen, pbInfo):
    graph = pbInfo.gdInfo.graph
    color = (0, 0, 192)
    string = "Graph colouring complete!"
    box = pbInfo.gdInfo.boundingBox
    textSurface = pbInfo.font.render(string, True, color)
    x = box.x + 16
    y = box.y + 16
    screen.blit(textSurface, (x,y))
    if pbInfo.gdInfo.numColorsUsed < pbInfo.colorTarget:
        string = "You found a solution with less colours than the target!"
    elif pbInfo.gdInfo.numColorsUsed == pbInfo.colorTarget:
        string = "You found a target solution, good job!"
    elif pbInfo.gdInfo.numColorsUsed > pbInfo.colorTarget:
        string = "Now try to find a colouring that matches the target."
    textSurface = pbInfo.fontSmall.render(string, True, color)
    y += textSurface.get_height() * 1.2
    screen.blit(textSurface, (x,y))

# Create a color wheel image on a surface
def create_color_wheel(radius, pbInfo):
    width = radius*2
    height = radius*2
    surface = pygame.Surface((width, height), pygame.SRCALPHA, 32)
    inUsedColorRect = False
    for x in range(0, width):
        for y in range(0, height):
            dx = x-radius
            dy = y-radius
            distance = math.sqrt(dx*dx + dy*dy)
            if (distance >= radius-2 or distance < pbInfo.colorWheelCentreRadius):
                if distance >= radius-2 and distance <= radius:
                    surface.set_at((x, y), (255,255,0))
                elif distance < pbInfo.colorWheelCentreRadius and distance > pbInfo.colorWheelCentreRadius-3:
                    surface.set_at((x, y), (255,255,0))
                continue
            else:
                # Get color at this position
                phi = math.degrees(math.atan2(dy, dx) + math.pi)
                # Check if color is allowed to be used
                if angle_allowed(phi, pbInfo.usedColorAngles, pbInfo.colorWheelUsedSize):
                    colorTemp = colorsys.hsv_to_rgb(phi/360, distance/radius, 1)
                else:
                    colorTemp = colorsys.hsv_to_rgb(phi/360, distance/radius, 0.4)
                color = (int(colorTemp[0]*255), int(colorTemp[1]*255), int(colorTemp[2]*255))
                surface.set_at((x, y), color)
    return surface.convert_alpha()

def draw_menu_box(screen, pbInfo):
    screen.blit(pbInfo.greySurface, (0,0))
    if pbInfo.showHelp:
        draw_help_box(screen, pbInfo)
    else:
        pygame.draw.rect(screen, pbInfo.innerColor, pbInfo.menuRect, 0)
        pygame.draw.rect(screen, pbInfo.borderColor, pbInfo.menuRect, pbInfo.borderWidth)

class UI_UserInput:
    def __init__(self, pbInfo):
        self.pbInfo = pbInfo
        self.child = None

    # Return true if mouse click was consumed
    def mouse_down(self, mousePos, button):
        if button > 3:
            # Still consume scrolls if in the menu
            return self.pbInfo.showMenu

        # If the menu is up, only that can be clicked on
        if self.pbInfo.showMenu:
            if self.pbInfo.showHelp:
                self.pbInfo.toggle_show_help()
                return True
            if not self.pbInfo.menuRect.collidepoint(mousePos):
                self.pbInfo.toggle_show_menu()
                return True
            else:
                for button in self.pbInfo.menuButtons:
                    if button.posRect.collidepoint(mousePos):
                        self.child = button
                        break
                return True

        if self.child is None:
            for button in self.pbInfo.allButtons:
                if button.posRect is not None and button.posRect.collidepoint(mousePos):
                    self.child = button
                    break
            if self.pbInfo.gdInfo.winnerPlayer != -1:
                for button in self.pbInfo.endgameButtons:
                    if button.posRect is not None and button.posRect.collidepoint(mousePos):
                        self.child = button
                        break
            # If not clicking on the colour wheel, hide it
            if self.child is not self.pbInfo.plusButton:
                if self.pbInfo.colorWheelVisible:
                    colorClicked = get_color_wheel_clicked(self.pbInfo, mousePos)
                    if colorClicked == False:
                        self.pbInfo.toggle_color_wheel_visiblity()
                    elif not colorClicked == (0,0,0):
                        if not colorClicked == (255,255,255):
                            # If the function returned white, then the centre was clicked
                            self.pbInfo.colorWheelCentreColor = colorClicked
                        else:
                            if self.pbInfo.colorWheelCentreColor is not None:
                                add_color_to_palette(self.pbInfo, self.pbInfo.colorWheelCentreColor)
                            self.pbInfo.toggle_color_wheel_visiblity()
                        return True
                    else:
                        return True
                            
            return self.child is not None
        return False
                
    def mouse_up(self, mousePos, button):
        if button > 3:
            return False
        if self.child is not None:
            if self.child.posRect.collidepoint(mousePos):
                self.child.pressed()
            self.child = None
        
