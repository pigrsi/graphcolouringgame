import pygame, igraph
import pygame.gfxdraw
import sys, math
from random import random
from computer_strategy import *
from utility import *
from load_graph import initialise_graph, save_graph, graph_completed

# Store info about the graph display
class GraphDisplayInfo:
    def __init__(self, boundingBox, graph, uncoloredColor, vertexRadius, makerBreakerMode, originalGraph, opponentLevel):
        self.boundingBox = boundingBox
        self.uncoloredColor = uncoloredColor
        self.selectedColor = uncoloredColor
        self.vertexRadius = vertexRadius
        self.zoomLevel = 1
        self.returnToMenu = False
        self.nextGraph = False
        self.beginExit = False
        self.paused = False
        self.exitDelay = 0.5
        self.graph = graph
        self.originalGraph = originalGraph
        self.mbMode = makerBreakerMode-1
        self.colors = set()
        self.completedSaved = False
        self.opponentLevel = opponentLevel
        self.reset(False)

    def reset(self, resetGraph=True):
        if resetGraph:
            self.graph = initialise_graph(self.graph)   
        if self.mbMode == 0:
            self.colors = set()
        self.set_paint_color(self.uncoloredColor)
        # For 2-player mode
        self.turnPlayer = 0
        self.winnerPlayer = -1
        self.computersTurn = False
        self.computerDelay = 0
        self.pannedToComputerVertex = False
        self.computerVertexColorPair = None
        self.allColored = False
        self.numColorsUsed = 0
        self.set_background_color()
        move_graph_to_centre(self.graph.vs, self)

    def set_paint_color(self, color):
        self.selectedColor = color

    def turn_complete(self):
        self.allColored = all_colored(self.graph)
        self.numColorsUsed = num_colors_used(self.graph)

        # Save graph completion
        # PvP game does not track completion
        if self.allColored and not self.completedSaved:
            if self.graph["isCustom"]:
                self.completedSaved = True
            elif self.mbMode == 0:
                if self.numColorsUsed <= self.pbInfo.colorTarget:
                    graph_completed(self.graph, True)
                    self.originalGraph["completed"] = '1' + self.originalGraph["completed"][1]
                    self.completedSaved = True
            elif self.mbMode == 2 and self.numColorsUsed <= self.pbInfo.colorTarget+1 and self.opponentLevel > 0:
                graph_completed(self.graph, False)
                self.originalGraph["completed"] = self.originalGraph["completed"][0] + '1'
                self.completedSaved = True

        if self.mbMode == 0:
            return

        if self.turnPlayer == 0:
            self.turnPlayer = 1
        else:
            self.turnPlayer = 0

        prevWinnerPlayer = self.winnerPlayer
        # Check if a player has won
        # If all vertices are coloured, maker wins
        if self.allColored:
            self.winnerPlayer = 0
        else:
            # If there exists a vertex that is unable to be colored with the
            # existing color set, breaker wins
            for v in self.graph.vs:
                if v["uncolorable"]:
                    self.winnerPlayer = 1
                    break
        self.set_background_color()

        self.computersTurn = (self.mbMode==2 and self.turnPlayer==1 and self.winnerPlayer == -1)

        if prevWinnerPlayer == -1 and self.winnerPlayer != -1:
            self.pbInfo.game_ended()

    def check_uncolorable_vertices(self):
        # Check if any are uncolorable
        for v in self.graph.vs:
            v["uncolorable"] = is_vertex_uncolorable(v, self.graph, self.colors)
            
    def update_color_set(self, newColor):
        self.colors.add(newColor)

    def set_background_color(self):
        if self.mbMode != 0:
            if self.winnerPlayer == 0:
                self.backgroundColor = (220,220,238)
            elif self.winnerPlayer == 1:
                self.backgroundColor = (238,220,220)
            elif self.turnPlayer == 0:
                self.backgroundColor = (220,220,238)
            else:
                self.backgroundColor = (238,220,220)
        else:
            self.backgroundColor = (238,245,238)

    def return_to_menu(self, nextGraph=False):
        self.beginExit = True
        self.nextGraph = nextGraph

def update_display(gdInfo, graph, delta):
    if gdInfo.beginExit:
        if gdInfo.exitDelay <= 0:
            gdInfo.returnToMenu = True
        gdInfo.exitDelay -= delta

    if gdInfo.paused:
        return
    update_vertices(graph.vs)

    if gdInfo.computersTurn:
        if gdInfo.computerVertexColorPair == None:
            gdInfo.computerVertexColorPair = computer_select_vertex(gdInfo, graph, gdInfo.opponentLevel)
            gdInfo.computerDelay = 0.75
        elif not gdInfo.pannedToComputerVertex:
            if gdInfo.computerDelay > 0:
                gdInfo.computerDelay -= delta
            else:
                box = gdInfo.boundingBox
                vPosition = (gdInfo.computerVertexColorPair[0]["x"], gdInfo.computerVertexColorPair[0]["y"])
                dx = box.centerx - vPosition[0]
                dy = box.centery - vPosition[1]
                pan_vertices(graph.vs, dx/20, dy/20, False)
                distance = math.sqrt(dx*dx + dy*dy)
                if distance <= 100:
                    gdInfo.pannedToComputerVertex = True
                    gdInfo.computerDelay = 1# + random()*1
        elif gdInfo.computerDelay <= 0:
            assign_vertex_color(gdInfo.computerVertexColorPair[0], gdInfo.computerVertexColorPair[1], True)
            gdInfo.check_uncolorable_vertices()
            gdInfo.turn_complete()
            gdInfo.computerVertexColorPair = None
            gdInfo.pannedToComputerVertex = False
        else:
            gdInfo.computerDelay -= delta

def update_vertices(vertices):
    # Move by speed
    for v in vertices:
        v["x"] += v["speedX"]
        v["y"] += v["speedY"]
        v["speedX"] /= 1.125
        v["speedY"] /= 1.125

def draw_graph(screen, vertices, edges, pulsateProgress, gdInfo):
    screen.fill(gdInfo.backgroundColor)
    draw_edges(screen, vertices, edges, gdInfo)
    return draw_vertices(screen, vertices, pulsateProgress, gdInfo)

# Takes array of igraph vertices and draws them to screen
def draw_vertices(screen, vertices, pulsateProgress, gdInfo):
    if not gdInfo.computersTurn:
        hovered_vertex = get_hovered_vertex(vertices, gdInfo)
    elif gdInfo.computerVertexColorPair != None and gdInfo.pannedToComputerVertex:
        hovered_vertex = gdInfo.computerVertexColorPair[0]
    else:
        hovered_vertex = None
    radiusOffset = 0
    for v in vertices:
        if v == hovered_vertex and not (gdInfo.mbMode and v["color"]!=v["uncolored_color"]):
            radiusOffset = int(5 * (math.cos(pulsateProgress)+1))
        else:
            radiusOffset = 0
        rad = gdInfo.vertexRadius+radiusOffset
        # Inner colour
        if v["color_fill"] < 1:
            pygame.gfxdraw.filled_circle(screen, int(v["x"]), int(v["y"]), rad, v["color_prev"])
        pygame.gfxdraw.filled_circle(screen, int(v["x"]), int(v["y"]), int(rad*v["color_fill"]), v["color"])
        if v["color_fill"] < 1:
            v["color_fill"] += 0.1
            if v["color_fill"] > 1:
                v["color_fill"] = 1
        # Outer colour
        pygame.gfxdraw.aacircle(screen, int(v["x"]), int(v["y"]), rad, (0,0,0))
        if gdInfo.mbMode!=0 and v["uncolorable"]:
            pygame.draw.circle(screen, (255,0,0), (int(v["x"]), int(v["y"])), rad+2, 4)

    if hovered_vertex:
        pulsateProgress += 0.075
    else:
        pulsateProgress = math.pi/0.75

    return pulsateProgress

# Returns whether the vertex was successfully coloured
def assign_vertex_color(vertex, color, mbRules=False):
    # In maker-breaker, once a vertex is coloured it cannot be changed or undone
    if mbRules:
        if vertex["color"] != vertex["uncolored_color"]:
            return False

    color_clash = False
    if vertex["color"] is not color:
        # Make sure adjacent nodes don't have the same colour
        for neighbor in vertex.neighbors():
            if color != vertex["uncolored_color"] and neighbor["color"] == color:
                graph = vertex.graph
                edge = graph.es[set(graph.incident(vertex)).intersection(set(graph.incident(neighbor))).pop()]
                edge["shake_timer"] = 1
                color_clash = True
        if not color_clash and color != vertex["color"]:
            vertex["color_prev"] = vertex["color"]
            vertex["color"] = color
            vertex["color_fill"] = 0
            return True
        return False

def draw_edges(screen, vertices, edges, gdInfo):
    hovered = None
    if not gdInfo.computersTurn:
        hovered = get_hovered_vertex(vertices, gdInfo)
    for e in edges:
        color = (0, 0, 0)
        width = 1
        if hovered is not None and e["shake_timer"] <= 0:
            if hovered == e.graph.vs[e.source] or hovered == e.graph.vs[e.target]:
                color = (128, 0, 255)
                width = 2
        a = e.tuple[0]
        b = e.tuple[1]
        x_offset = 0
        y_offset = 0
        if e["shake_timer"] > 0:
            e["shake_timer"] -= 0.015
            x_offset = -5 + 10 * random()
            y_offset = -5 + 10 * random()
            color = (255,0,0)

        a_pos = (int(vertices[a]["x"]+x_offset), int(vertices[a]["y"]+y_offset))
        b_pos = (int(vertices[b]["x"]+x_offset), int(vertices[b]["y"]+y_offset))
        pygame.draw.line(screen, color, a_pos, b_pos, width)
        #pygame.gfxdraw.line(screen, a_pos[0], a_pos[1], b_pos[0], b_pos[1], black)

def get_hovered_vertex(vertices, gdInfo):
    mousePos = pygame.mouse.get_pos()
    if not gdInfo.boundingBox.collidepoint(mousePos):
        return None
    for v in vertices:
        if point_is_within_vertex(mousePos, v, gdInfo.vertexRadius):
            return v
    return None

def pan_vertices(vertices, dx, dy, noAcceleration=False):
    if noAcceleration:
        for v in vertices:
            v["x"] += dx
            v["y"] += dy
    else:
        vertices["speedX"] = dx
        vertices["speedY"] = dy
    return True

def move_graph_to_centre(vertices, gdInfo):
    move_vertices_to_point(vertices, gdInfo.boundingBox.centerx, gdInfo.boundingBox.centery)

# Will give the effect of the graph zooming out
def shrink_graph(vertices, gdInfo, percentage):
    if gdInfo.zoomLevel+percentage < 0.5 or gdInfo.zoomLevel+percentage > 2:
        return
    converge_graph(vertices, percentage, gdInfo.boundingBox.center)
    gdInfo.zoomLevel += percentage

# Will give the effect of the graph zooming in
def grow_graph(vertices, gdInfo, percentage):
    shrink_graph(vertices, gdInfo, -percentage)

def rotate_graph(vertices, gdInfo, angle):
    graphCentre = get_graph_centre(vertices, True)
    for v in vertices:
        vx = v["x"]
        vy = v["y"]
        dx = graphCentre[0] - vx
        dy = graphCentre[1] - vy
        if dx == 0 and dy == 0:
            continue
        x2 = vx - graphCentre[0]
        y2 = vy - graphCentre[1]
        x3 = x2*math.cos(angle) - y2*math.sin(angle)
        y3 = x2*math.sin(angle) + y2*math.cos(angle)
        v["x"] = x3 + graphCentre[0]
        v["y"] = y3 + graphCentre[1]

# Returns the number of vertices with their centre points visible
def get_num_vertices_on_screen(vertices, gdInfo, stopAt=0):
    # stopAt > 0 will end the count prematurely
    count = 0
    rect = gdInfo.boundingBox
    for v in vertices:
        if rect.contains((v["x"],v["y"])):
            count += 1
    if stopAt > 0 and count == stopAt:
        return count
    return count  

class GraphUserInput:
    def __init__(self, gdInfo):
        self.IDLE = 0
        self.DRAGGING_CHILD = 1
        self.MOUSE_PANNING = 2
        self.COLORING_VERTEX = 3
        self.state = self.IDLE
        self.child = None
        self.gdInfo = gdInfo
        self.rightClick = False

        self.panningWithKeys = False
        self.lastPanningKey = None

        self.NOT_ZOOMING = 0
        self.ZOOMING_IN = 1
        self.ZOOMING_OUT = 2
        self.zoomState = self.NOT_ZOOMING

        self.NOT_ROTATING = 0
        self.ROTATING_RIGHT = 1
        self.ROTATING_LEFT = 2
        self.rotateState = self.NOT_ROTATING

    def mouse_down(self, mousePos, button, vertices):
        if (not self.gdInfo.boundingBox.collidepoint(mousePos) or self.gdInfo.computersTurn):
            return

        self.rightClick = (button == 3)
        if self.state == self.IDLE:
            if button == 1 or button == 3:
                vertex = self.selectVertex(vertices)
                if vertex is None:
                    self.state = self.MOUSE_PANNING
                else:
                    self.child = vertex

        # Scrolling up
        if button == 4:
            grow_graph(vertices, self.gdInfo, 0.02)
        # Scrolling down
        elif button == 5:
            shrink_graph(vertices, self.gdInfo, 0.02)
                
    def mouse_up(self, mousePos, vertices):
        #if (not self.gdInfo.boundingBox.collidepoint(mousePos) or self.gdInfo.computersTurn):
        #    return
        if self.child is not None and self.child == self.selectVertex(vertices):
            self.state = self.COLORING_VERTEX
        elif self.state == self.MOUSE_PANNING:
            self.state = self.IDLE

    def key_down(self, key, vertices):
        if key == pygame.K_ESCAPE or key == pygame.K_p:
            self.gdInfo.pbInfo.toggle_show_menu()
        if self.gdInfo.computersTurn or self.gdInfo.paused:
            return
        if key == pygame.K_KP_MINUS:
            self.zoomState = self.ZOOMING_OUT
        elif key == pygame.K_KP_PLUS:
            self.zoomState = self.ZOOMING_IN
        elif key == pygame.K_UP or key == pygame.K_DOWN or \
            key == pygame.K_LEFT or key == pygame.K_RIGHT:
            self.panningWithKeys = True
            self.lastPanningKey = key
        elif key == pygame.K_r:
            move_graph_to_centre(vertices, self.gdInfo)
        elif key == pygame.K_q:
            self.rotateState = self.ROTATING_LEFT
        elif key == pygame.K_e:
            self.rotateState = self.ROTATING_RIGHT
        elif key == pygame.K_END:
            save_graph(self.gdInfo.graph)

    def update(self, vertices):
        if self.gdInfo.computersTurn:
            return
        relative = pygame.mouse.get_rel()
        dx = relative[0]
        dy = relative[1]
        if self.state == self.MOUSE_PANNING:
            pan_vertices(vertices, dx, dy)
        elif self.state == self.COLORING_VERTEX:
            if self.child is not None:
                if self.rightClick:
                    vertexColored = assign_vertex_color(self.child, self.gdInfo.uncoloredColor, self.gdInfo.mbMode!=0)
                else:
                    vertexColored = assign_vertex_color(self.child, self.gdInfo.selectedColor, self.gdInfo.mbMode!=0)
                self.child = None
            if vertexColored:
                self.gdInfo.check_uncolorable_vertices()
                self.gdInfo.turn_complete()
                
            self.state = self.IDLE
        elif self.state == self.DRAGGING_CHILD:
            self.dragChild(dx, dy)

        # Check key panning
        if self.panningWithKeys:
            panned = False
            if pygame.key.get_pressed()[pygame.K_UP]: panned = pan_vertices(vertices, 0, 10, True)
            if pygame.key.get_pressed()[pygame.K_DOWN]: panned = pan_vertices(vertices, 0, -10, True)
            if pygame.key.get_pressed()[pygame.K_LEFT]: panned = pan_vertices(vertices, -10, 0, True)
            if pygame.key.get_pressed()[pygame.K_RIGHT]: panned = pan_vertices(vertices, 10, 0, True)
            if not panned:
                self.panningWithKeys = False

        # Check zoom in/out
        if self.zoomState == self.ZOOMING_IN:
            grow_graph(vertices, self.gdInfo, 0.01)
            if not pygame.key.get_pressed()[pygame.K_KP_PLUS]:
                self.zoomState = self.NOT_ZOOMING
        elif self.zoomState == self.ZOOMING_OUT:
            shrink_graph(vertices, self.gdInfo, 0.01)
            if not pygame.key.get_pressed()[pygame.K_KP_MINUS]:
                self.zoomState = self.NOT_ZOOMING

        # Check rotate
        if self.rotateState == self.ROTATING_LEFT:
            rotate_graph(vertices, self.gdInfo, 0.04)
            if not pygame.key.get_pressed()[pygame.K_q]:
                self.rotateState = self.NOT_ROTATING
        elif self.rotateState == self.ROTATING_RIGHT:
            rotate_graph(vertices, self.gdInfo, -0.04)
            if not pygame.key.get_pressed()[pygame.K_e]:
                self.rotateState = self.NOT_ROTATING

    def selectVertex(self, vertices):
        # Check if I have clicked on a vertex
        vertex = get_hovered_vertex(vertices, self.gdInfo)
        return vertex

    def dragChild(self, dx, dy):
        # Drag vertex child
        if self.child:
            self.child["x"] += dx
            self.child["y"] += dy




