import pygame, math
import utility

class Button:
    # startAtPos - button will be drawn in this position and move to its actual position
    def __init__(self, action, args=None, image=None, color=None, posRect=None, startAtPos=None, midPos=None, canPulsate=True, textSurface=None, fillColorImage=False, dontGrow=False, rectWidth=4):
        self.action = action
        self.image = image
        self.imageClean = None
        if self.image != None:
            self.imageClean = image.copy()
        if color == None:
            color = (255,255,255)
        self.fillColorImage = fillColorImage
        self.color = color
        self.posRect = posRect
        self.startPos = startAtPos
        self.startPosBackup = startAtPos
        self.dontGrow = dontGrow
        if posRect is not None and self.startPos is not None and midPos is None:
            self.midPos = ((self.startPos[0]+self.posRect.x)/2, self.startPos[1]-512)
        else:
            self.midPos = midPos
        self.movementProgress = 0
        self.args = args
        self.pulsate = False
        self.canPulsate = canPulsate
        self.sinCounter = 0
        self.firstDeltaSkipped = False
        self.textSurface = textSurface
        self.greyed = False
        self.rectWidth = rectWidth

    def pressed(self):
        if self.greyed:
            return
        # Unpack args list and use them as arguments
        self.action(*self.args)
        self.pulsate = self.canPulsate

    def set_position(self, posRect, midPos=None):
        self.posRect = posRect
        if midPos is None:
            if posRect is not None and self.startPos is not None:
                self.midPos = ((self.startPos[0]+self.posRect.x)/2, self.startPos[1]-512)
        else:
            self.midPos = midPos

    def reset_bezier(self):
        if self.startPosBackup == None or self.posRect == None:
            return
        self.startPos = self.startPosBackup
        self.sinCounter = 0

    def update(self, delta):
        if self.startPos is not None:
            if not self.firstDeltaSkipped:
                self.firstDeltaSkipped = True
            else:
                self.sinCounter += delta
                self.movementProgress = math.sin(self.sinCounter)
                if self.sinCounter >= math.pi/2:
                    self.startPos = None
                    self.sinCounter = 0
        elif self.pulsate:
            self.sinCounter += delta*10
            if self.sinCounter >= math.pi:
                self.sinCounter = 0
                self.pulsate = False

    def set_image(self, image):
        self.image = image
        self.imageClean = image

    def draw(self, screen):
        if self.posRect is None:
            return False

        drawRect = self.posRect
        col = self.color
        if self.greyed:
            col = (160,160,160)
        if col != None and self.imageClean != None and self.fillColorImage:
            self.image = self.imageClean.copy()
            self.image.fill(col, None, pygame.BLEND_RGBA_MULT)
        if self.startPos is not None:
            drawRect = drawRect.copy()
            newPos = utility.bezier2d(self.startPos, self.midPos, self.posRect, self.movementProgress)
            drawRect.x = newPos[0]
            drawRect.y = newPos[1]
            if not self.dontGrow:
                drawRect.width *= self.movementProgress
                drawRect.height *= self.movementProgress

        if self.image is not None:
            self.draw_scaled_image(screen, drawRect)
        elif col is not None:
            sin = self.get_pulsate_sin()
            scaledRect = (drawRect.x+sin/2,drawRect.y+sin/2,drawRect.width-sin,drawRect.height-sin)
            pygame.draw.rect(screen, col, scaledRect, 0)
            pygame.draw.rect(screen, (0,0,0), scaledRect, self.rectWidth)
        if self.textSurface is not None:
            self.draw_scaled_text(screen, drawRect)

    def draw_scaled_image(self, screen, drawRect):
        img = self.image
        sin = self.get_pulsate_sin()
        if self.pulsate:
            img = pygame.transform.smoothscale(img, (drawRect.width-sin,drawRect.height-sin))
        elif self.startPos is not None:
            img = pygame.transform.smoothscale(img, (drawRect.width,drawRect.height))
        screen.blit(img, (drawRect.x+sin/2, drawRect.y+sin/2))

    def draw_scaled_text(self, screen, drawRect):
        text = self.textSurface
        if not self.pulsate:
            screen.blit(text, (drawRect.centerx-text.get_width()/2, drawRect.centery-text.get_height()/2))
        else:
            sin = self.get_pulsate_sin()    
            text = pygame.transform.smoothscale(text, (text.get_width()-sin, text.get_height()-int(sin/2)))
            screen.blit(text, (drawRect.centerx-text.get_width()/2, drawRect.centery-text.get_height()/2))

    def get_pulsate_sin(self):
        if self.startPos != None:
            return 0
        return int(math.sin(self.sinCounter) * 16)

