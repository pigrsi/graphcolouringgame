import pygame
import igraph.vendor.texttable
from display_graph import *
from display_ui import *
from display_menu import *
from graph_builder import *
from graph_builder_ui import *
from greedy_solver import *
from load_graph import *

def run_graph(winWidth, winHeight, modeNumber, graph, rearrange=False, opponentLevel=1):
    originalGraph = graph
    graph = graph.copy()

    minColorList = start_greedy(graph, 50)
        
    target = len(set(minColorList))
    screen = pygame.display.set_mode((winWidth,winHeight), pygame.RESIZABLE)
    mouse_images = [pygame.image.load("../assets/cursor0_outer.png").convert_alpha(),
                    pygame.image.load("../assets/cursor0_inner.png").convert_alpha()]
    mouse_white_inner = mouse_images[1].copy()
    cursor_color = graph.vs[0]["uncolored_color"]
    mouse_images[1].fill(cursor_color, None, pygame.BLEND_RGBA_MULT)

    # Create info objects for Graph display and UI display
    gdInfo = GraphDisplayInfo(pygame.Rect(0, 0, screen.get_width(), screen.get_height()-128), graph, graph.vs[0]["uncolored_color"], 18, modeNumber, originalGraph, opponentLevel)
    pbInfo = PaletteBoxInfo(pygame.Rect(0, screen.get_height()-128, screen.get_width(), 128), (183,255,206), (45, 172, 39), 8, target, mainFont, gdInfo)
    gdInfo.pbInfo = pbInfo

    # Create user input objects
    graphInput = GraphUserInput(gdInfo)
    uiInput = UI_UserInput(pbInfo)

    if rearrange:
        fr = FruchtReiner(graph, 700, 700, 500)
        while rearrange and not fr.isFinished:
            fr.update_vertex_positions()
            move_graph_to_centre(graph.vs, gdInfo)
    else:
        move_graph_to_centre(graph.vs, gdInfo)
        
    clock = pygame.time.Clock()

    pulsateProgress = 0

    resizeOnUnclick = False
    resizeDelay = 0
    while 1:
        delta = clock.get_time() / 1000.0
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if not uiInput.mouse_down(event.pos, event.button):
                    graphInput.mouse_down(event.pos, event.button, graph.vs)
            elif event.type == pygame.MOUSEBUTTONUP:
                graphInput.mouse_up(event.pos, graph.vs)
                uiInput.mouse_up(event.pos, event.button)
            elif event.type == pygame.KEYDOWN:
                graphInput.key_down(event.key, graph.vs)
            elif event.type == pygame.VIDEORESIZE:
                winWidth = event.size[0]
                winHeight = event.size[1]
                if winWidth < 620:
                    winWidth = 620
                if winHeight < 600:
                    winHeight = 600
                resizeOnUnclick = True
                resizeDelay = reset_resize_delay()
                gdInfo.boundingBox = pygame.Rect(0, 0, winWidth, winHeight-128)
                pbInfo.updateSizes(pygame.Rect(0, winHeight-128, winWidth, 128))
            elif event.type == pygame.QUIT:
                sys.exit()

        if resizeOnUnclick:
            if resizeDelay <= 0 or pygame.mouse.get_focused():
                resizeOnUnclick = False
                move_graph_to_centre(graph.vs, gdInfo)
                screen = pygame.display.set_mode((winWidth,winHeight), pygame.RESIZABLE)
            else:
                resizeDelay -= delta

        graphInput.update(graph.vs)
        update_display(gdInfo, graph, delta)
        update_ui(pbInfo, delta)

        # Draw graph
        pulsateProgress = draw_graph(screen, graph.vs, graph.es, pulsateProgress, gdInfo)

        # Draw buttons and information
        draw_ui(screen, pbInfo)

        # Update cursor
        # Check if cursor color needs to change
        if pygame.mouse.get_focused():
            if cursor_color is not gdInfo.selectedColor:
                mouse_images[1] = mouse_white_inner.copy()
                mouse_images[1].fill(gdInfo.selectedColor, None, pygame.BLEND_RGBA_MULT)
                cursor_color = gdInfo.selectedColor
            screen.blit(mouse_images[1], pygame.mouse.get_pos())
            screen.blit(mouse_images[0], pygame.mouse.get_pos())

        if gdInfo.returnToMenu:
            gdInfo.returnToMenu = False
            return (winWidth, winHeight, gdInfo.nextGraph)

        pygame.display.flip()

        clock.tick(60)
            

def run_menu(winWidth, winHeight):
    screen = pygame.display.set_mode((winWidth,winHeight), pygame.RESIZABLE)
    mouse_images = [pygame.image.load("../assets/cursor0_outer.png").convert_alpha(),
                    pygame.image.load("../assets/cursor0_inner.png").convert_alpha()]
    mouse_images[1].fill((0,0,0), None, pygame.BLEND_RGBA_MULT)

    clock = pygame.time.Clock()

    mdInfo = MenuDisplayInfo(pygame.Rect(0,0,winWidth,winHeight), mainFont)
    menuInput = Menu_UserInput(mdInfo)
    resizeOnUnclick = False
    resizeDelay = 0
    while 1:
        delta = clock.get_time() / 1000.0
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                menuInput.mouse_down(event.pos, event.button)
            elif event.type == pygame.MOUSEBUTTONUP:
                menuInput.mouse_up(event.pos, event.button)
            elif event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.VIDEORESIZE:
                winWidth = event.size[0]
                winHeight = event.size[1]
                if winWidth < 620:
                    winWidth = 620
                if winHeight < 600:
                    winHeight = 600
                resizeOnUnclick = True
                resizeDelay = reset_resize_delay()
                mdInfo.boundingBox = pygame.Rect(0, 0, winWidth, winHeight)

        if resizeOnUnclick:
            if resizeDelay <= 0 or pygame.mouse.get_focused():
                resizeOnUnclick = False
                screen = pygame.display.set_mode((winWidth,winHeight), pygame.RESIZABLE)
                mdInfo.reset_bubbles()
                set_menu_buttons_positions(mdInfo)
            else:
                resizeDelay -= delta

        update_menu(mdInfo, delta)
        draw_menu(screen, mdInfo)

        if pygame.mouse.get_focused():
            screen.blit(mouse_images[1], pygame.mouse.get_pos())
            screen.blit(mouse_images[0], pygame.mouse.get_pos())

        pygame.display.flip()
        clock.tick(60)

        if (mdInfo.gotoMode != 0):
            # Right now run_graph runs as another infinite loop inside this infinite loop,
            # so if the inner loop is broken out of, it will return to the menu
            if mdInfo.gotoMode < 4:
                rearrange = False
                # While this is true, when this run_graph ends, run another one with the next graph
                nextGraphAfterThisOne = True
                nextGraph = mdInfo.selectedGraph
                opponentLevel = mdInfo.opponentLevel
                while nextGraphAfterThisOne:
                    nextGraphAfterThisOne = False
                    Result = run_graph(winWidth, winHeight, mdInfo.gotoMode, initialise_graph(nextGraph), rearrange, opponentLevel)
                    winSize = (Result[0], Result[1])
                    winWidth = winSize[0]
                    winHeight = winSize[1]
                    nextGraphAfterThisOne = Result[2]
                    if nextGraphAfterThisOne:
                        nextGraph = get_loaded_graph(nextGraph["graphNumber"]+1)
            else:
                winSize = run_graph_builder(winWidth, winHeight)
            load_graphs_from_file()
            load_completion_flags()
            mdInfo.gotoMode = 0
            winWidth = winSize[0]
            winHeight = winSize[1]
            if winWidth < 620:
                winWidth = 620
            if winHeight < 600:
                winHeight = 600
            resizeOnUnclick = True
            mdInfo.boundingBox = pygame.Rect(0, 0, winWidth, winHeight)

def run_graph_builder(winWidth, winHeight, graph=None):
    screen = pygame.display.set_mode((winWidth,winHeight), pygame.RESIZABLE)
    mouse_images = [pygame.image.load("../assets/cursor0_outer.png").convert_alpha(),
                    pygame.image.load("../assets/cursor0_inner.png").convert_alpha()]
    mouse_images[1].fill((0,0,0), None, pygame.BLEND_RGBA_MULT)
    mouse_images[1].blit(mouse_images[0], (0,0))

    # Create info objects for Graph display and UI display
    gbInfo = GraphBuilderInfo(pygame.Rect(0, 0, screen.get_width(), screen.get_height()-128), graph)
    gbuInfo = GraphBuilderUI(gbInfo, pygame.Rect(0, screen.get_height()-128, screen.get_width(), 128), mouse_images[1], mainFont)
    gbInfo.gbuInfo = gbuInfo

    # Create user input objects
    gbInput = GBUserInput(gbInfo)
    gbuInput = GBUUserInput(gbuInfo)

    clock = pygame.time.Clock()
    resizeOnUnclick = False
    resizeDelay = 0
    while 1:
        delta = clock.get_time() / 1000.0
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if not gbuInput.mouse_down(event.pos, event.button):
                    gbInput.mouse_down(event.pos, event.button)
            elif event.type == pygame.MOUSEBUTTONUP:
                gbInput.mouse_up(event.pos, event.button)
                gbuInput.mouse_up(event.pos, event.button)
            elif event.type == pygame.KEYDOWN:
                gbInput.key_down(event.key, gbInfo)
                gbuInput.key_down(event.key, gbInfo)
            elif event.type == pygame.VIDEORESIZE:
                winWidth = event.size[0]
                winHeight = event.size[1]
                if winWidth < 620:
                    winWidth = 620
                if winHeight < 600:
                    winHeight = 600
                resizeDelay = reset_resize_delay()
                resizeOnUnclick = True
                gbInfo.boundingBox = pygame.Rect(0, 0, winWidth, winHeight-128)
                gbuInfo.boundingBox = pygame.Rect(0, winHeight-128, winWidth, 128)
                gbuInfo.set_positions()
            elif event.type == pygame.QUIT:
                sys.exit()

        if resizeOnUnclick:
            if resizeDelay <= 0 or pygame.mouse.get_focused():
                resizeOnUnclick = False
                screen = pygame.display.set_mode((winWidth,winHeight), pygame.RESIZABLE)
            else:
                resizeDelay -= delta

        if gbuInfo.exit:
            return (winWidth, winHeight)

        gbInput.update()

        update_graph_builder(gbInfo)
        draw_graph_builder(screen, gbInfo)

        update_graph_builder_ui(delta, gbuInfo)
        draw_graph_builder_ui(screen, gbuInfo)

        pygame.display.flip()
        clock.tick(60)

def reset_resize_delay():
    return 0.1

pygame.init()
pygame.mouse.set_visible(False)
pygame.display.set_caption('Graph Colouring')
winWidth = 1150
winHeight = 700
load_default_graphs()
load_graphs_from_file()
load_completion_flags()
# Get the first font that is supported
fonts = ["laksaman", "loma", "notosansmonocjkkr", "verdana"]
mainFont = None
for font in fonts:
    for sysfont in pygame.font.get_fonts():
        if font == sysfont.lower():
            mainFont = sysfont
            break
    if mainFont != None:
        break
mainFont = "Sans"
run_menu(winWidth, winHeight)
