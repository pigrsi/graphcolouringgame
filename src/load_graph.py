import igraph
import random
import default_graphs

graph_creation_strings = []
custom_graph_strings = []
completion_flags = []
graph_storage_filename = "../assets/custom_graphs.txt"
completed_graphs_filename = "../assets/graphs_completed.txt"

def save_graph(graph):
    try:
        fileObject  = open(graph_storage_filename, "a")

        appendString = ""
        appendString += "[Graph " + str(len(graph_creation_strings)) + "]\n"
        appendString += "[Vertices]\n"
        for v in graph.vs:
            appendString += str(v["x"]) + " " + str(v["y"]) + "\n"
        appendString += "[Edges]\n"
        for e in graph.es:
            appendString += str(e.tuple[0]) + " " + str(e.tuple[1]) + "\n"
        appendString += "[Target]\n"
        appendString += str(graph["target"]) + "\n"
        # Add to list of already loaded graphs
        graph_creation_strings.append(appendString)
        appendString += "[End]\n"
        fileObject.write(appendString)
        fileObject.close()
    except:
        print("Graph could not be saved.")

# soloMode - boolean - False=Solomode True=Makerbreakermode
def graph_completed(graph, soloMode):
    try:
        with open(completed_graphs_filename, 'r') as file:
            data = file.readlines()

        graphNumber = graph["graphNumber"]
        if len(data[graphNumber]) < 2:
            return
        if soloMode:
            if data[graphNumber][0] == '1':
                return
            data[graphNumber] = str(1) + data[graphNumber][1] + '\n'
        else:
            if data[graphNumber][1] == '1':
                return
            data[graphNumber] = data[graphNumber][0] + str(1) + '\n'

        with open(completed_graphs_filename, 'w') as file:
            file.writelines(data)
    except:
        print("Could not open completed graphs file.")

def load_default_graphs():
    graph_string = ""
    for line in default_graphs.get_default_graph_strings().splitlines(True):
        if "[End]" in line:
            graph_creation_strings.append(graph_string)
            graph_string = ""
        else:
            graph_string += line
    
# Custom graphs    
def load_graphs_from_file():
    try:
        fileObject  = open(graph_storage_filename, "r") 
        global custom_graph_strings
        custom_graph_strings = []
        graph_string = ""
        # Put strings for each graph into list
        for line in fileObject.readlines():
            if "[End]" in line:
                custom_graph_strings.append(graph_string)
                graph_string = ""
            else:
                graph_string += line

        fileObject.close()
    except:
        print("Custom graphs file could not be found.")
        # Nothing to do if it fails, the null graph will show up

def load_completion_flags():
    try:
        with open(completed_graphs_filename, 'r') as file:
            data = file.read()

        global completion_flags
        completion_flags = []
        for line in data.splitlines():
            completion_flags.append(line)
    except:
        print("Could not load completion flags.")

def get_num_custom_graphs():
    return len(custom_graph_strings)

def get_loaded_graph_range(graphMin, graphMax, fromCustomSet=False):
    graph_strings = graph_creation_strings
    if fromCustomSet:
        graph_strings = custom_graph_strings
    graphs = []
    for i in range(graphMin, graphMax):
        if i >= len(graph_strings):
            break
        graphs.append(get_loaded_graph(i, fromCustomSet))
    return graphs

def get_null_custom_graph():
    return get_loaded_graph(-1, graphString=default_graphs.get_null_graph())

def get_loaded_graph(graphNumber, fromCustomSet=False, graphString=None):
    if graphString == None:
        graph_strings = graph_creation_strings
        if fromCustomSet:
            graph_strings = custom_graph_strings
        if graphNumber >= len(graph_strings):
            graphNumber = 0
        lines = graph_strings[graphNumber].splitlines()
    else:
        lines = graphString.splitlines()

    graph = igraph.Graph()

    GRAPH_START = 0
    VERTICES = 1
    EDGES = 2
    TARGET = 3
    END = 4
    readMode = GRAPH_START
    for line in lines:
        if readMode == GRAPH_START:
            if line == "[Vertices]":
                readMode = VERTICES
        elif readMode == VERTICES:
            if line == "[Edges]":
                readMode = EDGES
            else:
                # Add vertex
                graph.add_vertex()
                v = graph.vs[len(graph.vs)-1]
                splitPositions = line.split(" ", 1)
                v["x"] = float(splitPositions[0])
                v["y"] = float(splitPositions[1])
        elif readMode == EDGES:
            if line == "[Target]":
                readMode = TARGET
            else:
                splitPositions = line.split(" ", 1)
                graph.add_edge(int(splitPositions[0]), int(splitPositions[1]))
        elif readMode == TARGET:
            if line == "[End]":
                readMode = END
            else:
                graph["target"] = int(line)
        elif readMode == END:
            pass # Last line - does nothing here
    graph["graphNumber"] = graphNumber
    if not fromCustomSet and graphNumber >= 0 and graphNumber < len(completion_flags):
        graph["completed"] = completion_flags[graphNumber]
    else:
        graph["completed"] = "00"
    graph["isCustom"] = fromCustomSet
            
    return graph

def create_graph():
    famouses = ["Petersen", "Grotzsch", "Chvatal", "Coxeter", "Cubical", "Dodecahedral", "Meredith", "Tutte", "Walther"]

    graph = igraph.Graph.Famous(famouses[0])
    i = 0
    for vertex in graph.vs:
        vertex["x"] = i * 20
        vertex["y"] = i * 20
        i += 1
    
    initialise_graph(graph)

    return graph

def initialise_graph(graph):
    graph.vs["uncolored_color"] = [(255,255,255)]
    graph.vs["color_prev"] = graph.vs["uncolored_color"]
    graph.vs["color"] = graph.vs["uncolored_color"]
    graph.vs["color_fill"] = 1
    graph.vs["speedX"] = 0
    graph.vs["speedY"] = 0
    graph.vs["uncolorable"] = False
    graph.es["shake_timer"] = 0
    return graph

def create_crown_graph():
    graph = igraph.Graph()
    graph.add_vertices(8)
    for i in range(0, 4):
        for k in range(4, 8):
            if k != i+4:
                graph.add_edges([(i, k)])
    return graph
