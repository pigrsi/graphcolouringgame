config_filename = "../assets/config.txt"

def save_config(bubblesOn):
    try:
        fileObject  = open(config_filename, "w")
        if bubblesOn:
            string = "1"
        else:
            string = "0"

        fileObject.write(string)
        fileObject.close()
    except:
        print("Config could not be saved.")
    
def load_config():
    bubblesOn = True
    try: 
        fileObject = open(config_filename, "r") 
        for line in fileObject.readlines():
            if line == "1":
                bubblesOn = True
                break
            if line == "0":
                bubblesOn = False
                break

        fileObject.close()
    except:
        print("Config could not be loaded.")
        # Nothing needs to be done, will just return True
    return bubblesOn