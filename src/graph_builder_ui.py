import pygame
from ui_button import Button
from load_graph import save_graph, initialise_graph
from greedy_solver import start_greedy

class GraphBuilderUI:
    def __init__(self, graphBuilder, boundingBox, cursorImage, fontName):
        self.gbInfo = graphBuilder
        self.boundingBox = boundingBox
        self.borderColor = (120,120,220)
        self.innerColor = (180,180,220)
        self.borderWidth = 8
        # Create buttons
        self.buttons = []
        self.vertexButtonImages = [pygame.image.load("../assets/button_vertex_unpressed.png").convert_alpha(),
            pygame.image.load("../assets/button_vertex_pressed.png").convert_alpha()]
        self.edgeButtonImages = [pygame.image.load("../assets/button_edge_unpressed.png").convert_alpha(),
            pygame.image.load("../assets/button_edge_pressed.png").convert_alpha()]
        self.modeButtons = [Button(self.set_builder_mode, [1], self.vertexButtonImages[0], canPulsate=False),
                            Button(self.set_builder_mode, [2], self.edgeButtonImages[0], canPulsate=False)]
        self.buttons.extend(self.modeButtons)

        self.calcFont = pygame.font.Font(pygame.font.match_font(fontName), 30)
        self.calcText = self.calcFont.render("Num. colours calculated:", True, (45,0,128))
        self.graphTarget = 0

        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 24)
        text0 = buttonFont.render("Menu", True, (0,0,0))
        self.menuButton = Button(self.toggle_show_menu, [], color=(255,255,255), textSurface=text0)
        self.buttons.append(self.menuButton)

        self.saveGraphTexts = []
        self.saveGraphTexts.append(buttonFont.render("Save custom graph", True, (0,0,0)))
        self.saveGraphTexts.append(buttonFont.render("Saved!", True, (0,0,0)))
        text1 = buttonFont.render("Clear graph", True, (0,0,0))
        text2 = buttonFont.render("Exit to main menu", True, (0,0,0))
        self.saveGraphButton = Button(self.save_graph, [], color=(255,255,255), textSurface=self.saveGraphTexts[0])
        self.menuButtons = [self.saveGraphButton,
        Button(self.gbInfo.clear_graph, [], color=(255,255,255), textSurface=text1),
        Button(self.exit_to_menu, [], color=(255,255,255), textSurface=text2)]    

        self.set_positions()
        self.cursorImage = cursorImage
        self.beginExit = False
        self.exitDelay = 0.5
        self.exit = False
        self.showMenu = False
        self.recalculate_target()

    def save_graph(self):
        if len(self.gbInfo.graph.vs) > 0:
            save_graph(self.gbInfo.graph)
            self.saveGraphButton.textSurface = self.saveGraphTexts[1]

    def toggle_show_menu(self):
        self.showMenu = not self.showMenu
        self.set_builder_mode(0)
        self.saveGraphButton.textSurface = self.saveGraphTexts[0]

    def exit_to_menu(self):
        self.beginExit = True

    def set_positions(self):
        # Position vertex and edge buttons
        padding = 24
        buttonSize = (self.vertexButtonImages[0].get_width(), self.vertexButtonImages[0].get_height())
        midSectionWidth = padding + buttonSize[0] + padding + buttonSize[0] + padding
        midSectionLeft = self.boundingBox.centerx - midSectionWidth/2
        buttonYPos = self.boundingBox.centery - buttonSize[1]/2 - 4
        self.modeButtons[0].posRect = pygame.Rect(midSectionLeft+padding, buttonYPos, buttonSize[0], buttonSize[1])
        self.modeButtons[1].posRect = pygame.Rect(self.modeButtons[0].posRect.right+padding, buttonYPos, buttonSize[0], buttonSize[1])
        self.menuButton.posRect = pygame.Rect(self.boundingBox.right-buttonSize[0]-padding, buttonYPos, buttonSize[0], buttonSize[1])
        menuSize = (260, 260)
        menuPos = (self.gbInfo.boundingBox.centerx - menuSize[0]/2, self.gbInfo.boundingBox.centery - menuSize[1]/2)
        self.menuRect = pygame.Rect(menuPos[0], menuPos[1], menuSize[0], menuSize[1])
        greyHeight = self.gbInfo.boundingBox.height + self.boundingBox.height
        self.greySurface = pygame.Surface((self.gbInfo.boundingBox.width, greyHeight), pygame.SRCALPHA)
        self.greySurface.fill((0,0,0,90))
        buttonSize = (menuSize[0]-30, 48)
        padding = 16
        menuButtonsHeight = padding*(len(self.menuButtons)+2) + buttonSize[1]*len(self.menuButtons)
        prevBottom = menuPos[1]+menuSize[1]/2-menuButtonsHeight/2
        for button in self.menuButtons:
            rect = pygame.Rect(menuPos[0]+menuSize[0]/2-buttonSize[0]/2, prevBottom+padding, buttonSize[0], buttonSize[1])
            button.posRect = rect
            prevBottom = rect.bottom

    def set_builder_mode(self, modeNumber):
        if self.gbInfo.builderMode == modeNumber:
            modeNumber = 0
        self.gbInfo.builderMode = modeNumber
        self.gbInfo.selectedVertex = None
        self.gbInfo.draggingVertex = None
        vertexButtonPressed = (modeNumber == 1)
        edgeButtonPressed = (modeNumber == 2)
        if vertexButtonPressed:
            self.modeButtons[0].image = self.vertexButtonImages[1]
        else:
            self.modeButtons[0].image = self.vertexButtonImages[0]
        if edgeButtonPressed:
            self.modeButtons[1].image = self.edgeButtonImages[1]
        else:
            self.modeButtons[1].image = self.edgeButtonImages[0]

    def recalculate_target(self):
        self.graphTarget = len(set(start_greedy(initialise_graph(self.gbInfo.graph), 10)))
        self.gbInfo.graph["target"] = self.graphTarget

def update_graph_builder_ui(delta, gbuInfo):
    for button in gbuInfo.buttons:
        button.update(delta)
    if gbuInfo.showMenu:
        for button in gbuInfo.menuButtons:
            button.update(delta)

    if gbuInfo.beginExit:
        if gbuInfo.exitDelay <= 0:
            gbuInfo.exit = True
        gbuInfo.exitDelay -= delta

def draw_graph_builder_ui(screen, gbuInfo):
    draw_bottom_box(screen, gbuInfo)
    draw_window_border(screen, gbuInfo)
    
    for button in gbuInfo.buttons:
        button.draw(screen)
    if gbuInfo.showMenu:
        draw_menu_box(screen, gbuInfo)
        for button in gbuInfo.menuButtons:
            button.draw(screen)

    pos = pygame.mouse.get_pos()
    if not (gbuInfo.gbInfo.builderMode == 1 and gbuInfo.gbInfo.boundingBox.collidepoint(pos)):
        screen.blit(gbuInfo.cursorImage, pos)

    draw_left_text(screen, gbuInfo)
    
def draw_bottom_box(screen, gbuInfo):
    pygame.draw.rect(screen, gbuInfo.innerColor, gbuInfo.boundingBox, 0)
    pygame.draw.line(screen, gbuInfo.borderColor, (0,gbuInfo.boundingBox.y), 
                    (gbuInfo.boundingBox.width,gbuInfo.boundingBox.y), gbuInfo.borderWidth)

def draw_window_border(screen, gbuInfo):
    w2 = gbuInfo.borderWidth/2
    right = gbuInfo.boundingBox.right
    bottom = gbuInfo.boundingBox.bottom
    pygame.draw.line(screen, gbuInfo.borderColor, (0,0),(right,0), gbuInfo.borderWidth)
    pygame.draw.line(screen, gbuInfo.borderColor, (right-w2,0),(right-w2,bottom), gbuInfo.borderWidth)
    pygame.draw.line(screen, gbuInfo.borderColor, (right,bottom-w2),(0,bottom-w2), gbuInfo.borderWidth)
    pygame.draw.line(screen, gbuInfo.borderColor, (w2-1,bottom),(w2-1,0), gbuInfo.borderWidth)

def draw_menu_box(screen, gbuInfo):
    screen.blit(gbuInfo.greySurface, (0,0))
    pygame.draw.rect(screen, gbuInfo.innerColor, gbuInfo.menuRect, 0)
    pygame.draw.rect(screen, gbuInfo.borderColor, gbuInfo.menuRect, gbuInfo.borderWidth)

def draw_left_text(screen, gbuInfo):
    box = gbuInfo.boundingBox
    text = gbuInfo.calcFont.render(str(gbuInfo.graphTarget), True, (45,0,128))
    screen.blit(gbuInfo.calcText, (box.x+32, box.y+24))
    screen.blit(text, (box.x+gbuInfo.calcText.get_width()/2, box.y+72))

class GBUUserInput:
    def __init__(self, gbuInfo):
        self.child = None
        self.gbuInfo = gbuInfo

    def mouse_down(self, mousePos, button):
        if button > 3:
            return True

        if self.gbuInfo.showMenu:
            if not self.gbuInfo.menuRect.collidepoint(mousePos):
                self.gbuInfo.toggle_show_menu()
                return True
            else:
                for button in self.gbuInfo.menuButtons:
                    if button.posRect.collidepoint(mousePos):
                        self.child = button
                        break
                return True

        for button in self.gbuInfo.buttons:
            if button.posRect.collidepoint(mousePos):
                self.child = button

    def mouse_up(self, mousePos, button):
        if self.child is not None:
            if self.child.posRect.collidepoint(mousePos):
                self.child.pressed()
            self.child = None

    def key_down(self, key, gbInfo):
        if key == pygame.K_RETURN:
            self.gbuInfo.exit = True
            

