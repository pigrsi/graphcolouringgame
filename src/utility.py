from random import randint, random
import math

def get_random_distinct_color(alreadyUsed=[], getList=False):
    distinctColors = [(230, 25, 75),(60, 180, 75),(255, 225, 25),(0, 130, 200),(245, 130, 48),
    (240, 50, 230),(210, 245, 60),(250, 190, 190),(170, 110, 40),
    (128, 0, 0),(0, 0, 128),(24,24,24)]
    if getList:
        return distinctColors
    distinctColors = [i for i in distinctColors if not i in alreadyUsed]
    if len(distinctColors) == 0:
        return (random()*255, random()*255, random()*255)
    return distinctColors[randint(0, len(distinctColors)-1)]

# quadratic bezier
def bezier2d(start, mid, end, t):
    x = sq(1-t)*start[0] + 2*(1-t)*t*mid[0] + sq(t)*end[0]
    y = sq(1-t)*start[1] + 2*(1-t)*t*mid[1] + sq(t)*end[1]
    return (x, y)

def sq(x):
    return x*x

def get_allowed_colors_for_vertex(colors, graph, vertex):
    if vertex["color"] != vertex["uncolored_color"]:
        return set()
    colors = colors.copy()
    for neighbor in vertex.neighbors():
        if neighbor["color"] in colors:
            colors.remove(neighbor["color"])
    return colors

# Is it impossible to color this vertex using the current palette?
def is_vertex_uncolorable(vertex, graph, paletteSet):
    if vertex["color"] != vertex["uncolored_color"]:
        # Already coloured
        return False
    tempColors = paletteSet.copy()
    for u in graph.neighbors(vertex):
        if graph.vs[u]["color"] in tempColors:
            tempColors.remove(graph.vs[u]["color"])
    if len(tempColors) == 0:
        return True
    return False

# Biased - area with more vertices will be pushed to the centre more
def get_graph_centre(vertices, biased=False):
    # cog = centre of gravity
    cogX = 0
    cogY = 0
    if biased:
        for v in vertices:
           cogX += v["x"]
           cogY += v["y"]
        cogX /= len(vertices)
        cogY /= len(vertices)
    else:
        minPos = [None, None]
        maxPos = [None, None]
        for v in vertices:
            if minPos[0] == None or v["x"] < minPos[0]:
                minPos[0] = v["x"]
            if minPos[1] == None or v["y"] < minPos[1]:
                minPos[1] = v["y"]
            if maxPos[0] == None or v["x"] > maxPos[0]:
                maxPos[0] = v["x"]
            if maxPos[1] == None or v["y"] > maxPos[1]:
                maxPos[1] = v["y"] 
        cogX = (maxPos[0]+minPos[0])/2
        cogY = (maxPos[1]+minPos[1])/2
    return (cogX, cogY)

def move_vertices_to_point(vertices, centreX, centreY, graphCentre=None):
    # Find average of all vertex positions
    if graphCentre == None:
        graphCentre = get_graph_centre(vertices)
    dx = centreX - graphCentre[0]
    dy = centreY - graphCentre[1]
    for v in vertices:
        v["x"] += dx
        v["y"] += dy

# Will give the effect of the graph zooming out
def converge_graph(vertices, percentage, centre):
    # zoom relative to centre
    for v in vertices:
        # find distance to centre
        dx = centre[0] - v["x"]
        dy = centre[1] - v["y"]
        v["x"] += dx * percentage
        v["y"] += dy * percentage

def point_is_within_vertex(point, vertex, vRadius):
    # Check if distance between points is > than radius
    vPoint = (vertex["x"], vertex["y"])
    r2 = vRadius * vRadius
    distance = (vPoint[0]-point[0])*(vPoint[0]-point[0]) + \
        (vPoint[1]-point[1])*(vPoint[1]-point[1])
    return distance <= r2

def point_is_within_circle(point, circleCentre, circleRadius):
    # Check if distance between points is > than radius
    r2 = circleRadius * circleRadius
    distance = (circleCentre[0]-point[0])*(circleCentre[0]-point[0]) + \
        (circleCentre[1]-point[1])*(circleCentre[1]-point[1])
    return distance <= r2

def vertex_seq_to_list(vertexSeq):
    vertexList = []
    for v in vertexSeq:
        vertexList.append(v)
    return vertexList

def round_to_nearest(number, base):
    return int(base * round(float(number)/base))

def circles_overlap(pos1, rad1, pos2, rad2):
    distance = math.sqrt(sq(pos1[0]-pos2[0]) + sq(pos1[1]-pos2[1]))
    return distance < rad1+rad2

# The number of colours that are already taken by neighbours
def number_of_colors_sort_key(vertex):
    usedColors = set()
    for v in vertex.neighbors():
        if v["color"] != v["uncolored_color"]:
            usedColors.add(v["color"])
    return len(usedColors)

def degree_sort_key(vertex):
    return vertex.degree()

def vertices_to_list(vs):
    lst = []
    for v in vs:
        lst.append(v)
    return lst

def get_mutual_vertices(vs0, vs1):
    # Loop through the shorter list
    if len(vs1) < len(vs0):
        tmp = vs0
        vs0 = vs1
        vs1 = tmp
    mutuals = []
    for v in vs0:
        if v in vs1:
            mutuals.append(v)
    return mutuals

# Given two vertices, which colours can they both be assigned legally at this time?
def get_mutual_usable_colors(v0, v1, graph, palette):
    colors = set()
    for c1 in get_allowed_colors_for_vertex(palette, graph, v0):
        if c1 in get_allowed_colors_for_vertex(palette, graph, v1):
            colors.add(c1)
    return colors
            
def all_colored(graph):
    for v in graph.vs:
        if v["color"] == v["uncolored_color"]:
            return False
    return True

def num_colors_used(graph):
    colors = set()
    for v in graph.vs:
        if v["color"] != v["uncolored_color"]:
            colors.add(v["color"])
    return len(colors)
