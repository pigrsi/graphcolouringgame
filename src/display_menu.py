import pygame, igraph
import pygame.gfxdraw
import sys, math
from random import random
import utility, load_graph, load_config
from ui_button import Button

class MenuDisplayInfo:
    def __init__(self, boundingBox, fontName):
        self.boundingBox = boundingBox
        self.bubbles = self.create_bubbles_list((self.boundingBox.width*self.boundingBox.height)/5000)
        self.menuPage = 0
        self.titleFont = pygame.font.Font(pygame.font.match_font(fontName), 64)
        self.currentMenuNumElements = 0
        numGraphSets = 4
        graphSetSize = 10

        # Set up text to display on the buttons
        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 48)
        soloText = buttonFont.render("Solo", True, (0,0,0))
        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 32)
        mbText = buttonFont.render("Maker Breaker", True, (0,0,0))
        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 27)
        pvpText = buttonFont.render("Player vs Player", True, (0,0,0))
        pvmText = buttonFont.render("Player vs Computer", True, (0,0,0))
        self.graphSetTexts = self.create_graph_set_text(buttonFont, numGraphSets, graphSetSize)
        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 22)
        gbText = buttonFont.render("Graph Builder", True, (0,0,0))
        customGraphsText = buttonFont.render("Custom graphs", True, (0,0,0))
        backText = buttonFont.render("Back", True, (0,0,0))
        difficultyText = buttonFont.render("Opponent level:", True, (0,0,0))
        buttonFont = pygame.font.Font(pygame.font.match_font(fontName), 15)
        simpleText = buttonFont.render("Simple", True, (0,0,0))
        aggressiveText = buttonFont.render("Aggressive", True, (0,0,0))
        sneakyText = buttonFont.render("Sneaky", True, (0,0,0))
        bubblesText = buttonFont.render("Toggle bubbles", True, (0,0,0))
        arrowImageRight = pygame.image.load("../assets/next_button.png")
        arrowImageLeft = pygame.transform.flip(arrowImageRight, True, False)

        # Create button lists for each part of the menu
        # Main menu
        buttons_0 = [MenuButton(change_menu, [self, 2, 1, numGraphSets], soloText, True), MenuButton(change_menu, [self, 1], mbText, True),
                    MenuButton(goto_graph_mode, [self, 4], gbText)]
        # Maker breaker - choose opponent
        buttons_1 = [MenuButton(change_menu, [self, 2, 2, numGraphSets], pvpText), MenuButton(change_menu, [self, 2, 3, numGraphSets], pvmText),
        Button(self.set_opponent_level, [0], textSurface=simpleText, dontGrow=True, rectWidth=2),
        Button(self.set_opponent_level, [1], textSurface=aggressiveText, dontGrow=True, rectWidth=2),
        Button(self.set_opponent_level, [2], textSurface=sneakyText, dontGrow=True, rectWidth=2)]
        self.opponentLevelText = FloatingText(difficultyText)
        # Graph set selection
        buttons_2 = [MenuButton(graph_set_selected, [self, graphSetSize, (256,256), 3], self.graphSetTexts[0]), 
        MenuButton(change_item_index, [self, 2, -1], arrowImageLeft, color=(210,210,210)),
        MenuButton(change_item_index, [self, 2, 1], arrowImageRight, color=(210,210,210)),
        MenuButton(graph_set_selected, [self, 0, (256,256), 3, True], customGraphsText)]
        # Select graph in set
        buttons_3 = [MenuButton(goto_graph_mode, [self]), 
        MenuButton(change_item_index, [self, 3, -1], arrowImageLeft, color=(210,210,210)), 
        MenuButton(change_item_index, [self, 3, 1], arrowImageRight, color=(210,210,210))]
        self.backButton = MenuButton(prev_menu, [self], backText)
        buttons_1.append(self.backButton)
        buttons_2.append(self.backButton)
        buttons_3.append(self.backButton)

        # Some menus have multiple choices - this stores which item it is currently on 
        self.itemIndex = [0, 0, 0, 0]
        self.menuTitles = ("Graph Colouring", "Select opponent", "Select graph set", "Select graph")
        self.buttonPositionSetters = (set_buttons_positions_0, set_buttons_positions_1, set_buttons_positions_2, set_buttons_positions_3)
        self.currentGraphSet = None
        self.currentGraphSetImages = None
        self.selectedGraph = None

        self.allButtons = [buttons_0, buttons_1, buttons_2, buttons_3]
        self.graphFlagButton = MenuButton(None, [], image=pygame.image.load("../assets/checkmark.png"), imageOnly=True)
        self.displayBubbles = load_config.load_config()
        self.toggleBubblesButton = Button(self.toggle_bubbles, [], textSurface=bubblesText, dontGrow=True, rectWidth=2)
        set_buttons_positions_0(self)
        self.set_opponent_level(1)
        # Mode 1: Single player
        # Mode 2: Maker-breaker PvP
        # Mode 3: Maker-breaker PvAI
        self.nextMode = 0
        self.gotoMode = 0

    def toggle_bubbles(self):
        self.displayBubbles = not self.displayBubbles
        load_config.save_config(self.displayBubbles)
        
    def create_graph_set_text(self, font, numSets, setSize):
        texts = []
        for setNumber in range(0, numSets):
            string = "Graphs " + str(1+setNumber*setSize) + " to " + str(setSize+setSize*setNumber)
            texts.append(font.render(string, True, (0,0,0)))
        return texts

    def create_bubbles_list(self, numBubbles):
        if numBubbles > 200:
            numBubbles = 200
        bubs = []
        for i in range(0, int(numBubbles)):
            bubs.append(self.create_single_bubble())
        return bubs
    
    def create_single_bubble(self):
        radius = 16 + 48*random()
        x = int((self.boundingBox.x-radius/2) + (self.boundingBox.width+radius)*random())
        y = int((self.boundingBox.y-radius/2) + (self.boundingBox.height+radius)*random())
        bubDict = {'x': int(x), 'y': int(y), 'radius': radius, 'sin_timer': random()*2*math.pi, 'color': (255,255,255)}
        return bubDict

    def change_num_bubbles(self, newNum):
        newNum = int(newNum)
        # Remove unneeded
        if newNum < len(self.bubbles):
            self.bubbles = self.bubbles[0: newNum]

        # Reset bubble positions
        for b in self.bubbles:
            radius = b["radius"]
            b["x"] = int((self.boundingBox.x-radius/2) + (self.boundingBox.width+radius)*random())
            b["y"] = int((self.boundingBox.y-radius/2) + (self.boundingBox.height+radius)*random())

        # Create new
        if newNum > len(self.bubbles):
            for i in range(0, int(newNum-len(self.bubbles))):
                self.bubbles.append(self.create_single_bubble())

    def reset_bubbles(self):
        self.change_num_bubbles((self.boundingBox.width*self.boundingBox.height)/5000)

    def set_opponent_level(self, level):
        self.opponentLevel = level
        for i in range(2, 5):
            if i-2 == level:
                color = (0,240,0)
            else:
                color = (255,255,255)
            buttons = self.allButtons[1]
            buttons[i].color = color

def update_menu(mdInfo, delta):
    if mdInfo.menuPage == 2:
        # Set button text
        if mdInfo.itemIndex[2] < len(mdInfo.graphSetTexts):
            mdInfo.allButtons[2][0].image = mdInfo.graphSetTexts[mdInfo.itemIndex[2]]
    elif mdInfo.menuPage == 3:
        # Set button graph thumbnail
        if mdInfo.itemIndex[3] < len(mdInfo.currentGraphSetImages):
            mdInfo.allButtons[3][0].image = mdInfo.currentGraphSetImages[mdInfo.itemIndex[3]]
        else:
            mdInfo.allButtons[3][0].image = None

    for button in mdInfo.allButtons[mdInfo.menuPage]:
        button.update(delta)
    mdInfo.toggleBubblesButton.update(delta)

    if mdInfo.displayBubbles and not pygame.mouse.get_pos() == (0, 0):
        check_bubbles_hovered(mdInfo.bubbles)
    
def check_bubbles_hovered(bubbles):
    mousePos = pygame.mouse.get_pos()
    for bubble in bubbles:
        if utility.point_is_within_circle(mousePos, (bubble["x"], bubble["y"]), bubble["radius"]):
            if bubble["color"] == (255,255,255):
                bubble["color"] = (255*random(), 255*random(), 255*random())

def draw_menu(screen, mdInfo):
    screen.fill((245,245,245))
    if mdInfo.displayBubbles:
        for b in mdInfo.bubbles:
            rad_offset = math.sin(b["sin_timer"])*8
            b["sin_timer"] = b["sin_timer"] + 0.03
            pygame.gfxdraw.filled_circle(screen, b["x"], b["y"], int(b["radius"]+rad_offset), b["color"])
            pygame.gfxdraw.aacircle(screen, b["x"], b["y"], int(b["radius"]+rad_offset), (0,0,0))

    page = mdInfo.menuPage
    if page == 2 or page == 3:
        if mdInfo.currentMenuNumElements <= 1:
            mdInfo.allButtons[page][1].set_color((255,255,255))
            mdInfo.allButtons[page][2].set_color((255,255,255))
        elif mdInfo.itemIndex[page]-1 < 0:
            mdInfo.allButtons[page][1].set_color((255,255,255))
            mdInfo.allButtons[page][2].set_color((210,210,210))
        elif mdInfo.itemIndex[page]+1 >= mdInfo.currentMenuNumElements:
            mdInfo.allButtons[page][1].set_color((210,210,210))
            mdInfo.allButtons[page][2].set_color((255,255,255))
        else:
            mdInfo.allButtons[page][1].set_color((210,210,210))
            mdInfo.allButtons[page][2].set_color((210,210,210))
    for button in mdInfo.allButtons[mdInfo.menuPage]:
        button.draw(screen)

    draw_title_box(screen, mdInfo, mdInfo.menuTitles[mdInfo.menuPage])
    if mdInfo.menuPage == 3:
        if mdInfo.itemIndex[3] < len(mdInfo.currentGraphSet):
            if mdInfo.nextMode == 1:
                if mdInfo.currentGraphSet[mdInfo.itemIndex[3]]["completed"][0] == '1':
                    mdInfo.graphFlagButton.draw(screen)
            elif mdInfo.nextMode == 3:
                if mdInfo.currentGraphSet[mdInfo.itemIndex[3]]["completed"][1] == '1':
                    mdInfo.graphFlagButton.draw(screen)
    
    mdInfo.toggleBubblesButton.draw(screen)
    
    if page == 1:
        mdInfo.opponentLevelText.draw(screen)
    

def prev_menu(mdInfo):
    if mdInfo.menuPage == 1:
        mdInfo.menuPage = 0
    elif mdInfo.menuPage == 2:
        if mdInfo.nextMode <= 1:
            mdInfo.menuPage = 0
        else:
            mdInfo.menuPage = 1
    elif mdInfo.menuPage == 3:
        mdInfo.menuPage = 2
    else:
        return
    mdInfo.buttonPositionSetters[mdInfo.menuPage](mdInfo)
    mdInfo.currentMenuNumElements = 4

def change_menu(mdInfo, menuNumber, mode=None, numElements=None):
    if mode != None:
        mdInfo.nextMode = mode
    if numElements != None:
        mdInfo.currentMenuNumElements = numElements
    mdInfo.menuPage = menuNumber
    mdInfo.buttonPositionSetters[menuNumber](mdInfo)
    mdInfo.itemIndex[3] = 0

def graph_set_selected(mdInfo, graphSetSize, surfaceSize, nextMenu, fromCustomSet=False):
    if fromCustomSet:
        graphSetSize = load_graph.get_num_custom_graphs()
        if graphSetSize == 0:
            mdInfo.currentGraphSet = [load_graph.get_null_custom_graph()]
        else:
            mdInfo.currentGraphSet = load_graph.get_loaded_graph_range(0, graphSetSize, True)
            mdInfo.currentGraphSet.reverse()
    else:
        graphSetNo = mdInfo.itemIndex[2]
        mdInfo.currentGraphSet = load_graph.get_loaded_graph_range(graphSetNo*graphSetSize, (graphSetNo*graphSetSize)+graphSetSize)
    mdInfo.currentGraphSetImages = create_tiny_graph_surfaces(mdInfo.currentGraphSet, surfaceSize)
    mdInfo.currentMenuNumElements = graphSetSize
    change_menu(mdInfo, nextMenu)

def change_item_index(mdInfo, menuNumber, increment, maxIndex=None, minIndex=0):
        index = mdInfo.itemIndex[menuNumber] + increment
        if index < minIndex:
            index = minIndex
        elif index >= mdInfo.currentMenuNumElements:
            index = mdInfo.currentMenuNumElements-1
        mdInfo.itemIndex[menuNumber] = index

def set_menu_buttons_positions(mdInfo):
    mdInfo.buttonPositionSetters[mdInfo.menuPage](mdInfo)
    # Set toggle bubbles button position (appears on every screen)
    rect = mdInfo.boundingBox.copy()
    w = 96 # width & height
    p = 28 # padding
    mdInfo.toggleBubblesButton.posRect = pygame.Rect(rect.right-w-p, p, w, w)
    
class MenuButton:
    def __init__(self, action, args=None, image=None, circular=False, posRect=None, color=None, shapeFillColor=(255,255,255), imageOnly=False):
        self.action = action
        self.posRect = posRect
        self.args = args
        self.circular = circular
        self.image = image
        self.imageClean = None
        if image != None:
            self.imageClean = image.copy()
        self.hovered_growth = 0
        self.hovered = False
        self.color = color
        self.imageOnly = imageOnly
        if self.image != None and color != None:
            self.image = self.image.copy()
            self.image.fill(color, None, pygame.BLEND_RGBA_MULT)
        self.shapeFillColor = shapeFillColor

    def update(self, delta):
        mousePos = pygame.mouse.get_pos()
        if self.circular:
            self.hovered = utility.point_is_within_circle(mousePos, self.posRect, self.posRect.width/2)
        else:
            self.hovered = self.posRect.collidepoint(mousePos)

        if not self.hovered:
            self.hovered_growth = 0
        else:
            self.hovered_growth += delta*2
            if self.hovered_growth > 1:
                self.hovered_growth = 1

    def pressed(self):
        # Unpack args list and use them as arguments
        if self.action != None:
            self.action(*self.args)

    def draw(self, screen):
        if not self.imageOnly:
            rect = self.posRect
            if self.circular:            
                pygame.gfxdraw.filled_circle(screen, int(rect.x), int(rect.y), int(rect.width/2), self.shapeFillColor)
                pygame.gfxdraw.aacircle(screen, int(rect.x), int(rect.y), int(rect.width/2), (0,0,0))
                pygame.gfxdraw.aacircle(screen, int(rect.x), int(rect.y), int(rect.width/2)+1, (0,0,0))
                if self.hovered:
                    pygame.gfxdraw.aacircle(screen, rect.x, rect.y, int((rect.width/2)+18*self.hovered_growth), (0,0,0))
                    pygame.gfxdraw.aacircle(screen, rect.x, rect.y, int((rect.width/2)+18*self.hovered_growth/2), (0,0,0))
                    pygame.gfxdraw.aacircle(screen, rect.x, rect.y, int((rect.width/2)-18*self.hovered_growth/2), (0,0,0))
            else:
                pygame.draw.rect(screen, self.shapeFillColor, rect, 0)
                pygame.draw.rect(screen, (0,0,0), rect, 2)
                if self.hovered:
                    rect = rect.copy()
                    self.draw_rect_effect(screen, -16, rect)
                    self.draw_rect_effect(screen, 32, rect)
                    self.draw_rect_effect(screen, 16, rect)
        self.draw_image(screen)

    def draw_rect_effect(self, screen, growBy, rect):
        rect.width += (growBy*self.hovered_growth)
        rect.height += (growBy*self.hovered_growth)
        rect.x -= (growBy*self.hovered_growth/2-1)
        rect.y -= (growBy*self.hovered_growth/2-1)
        pygame.draw.rect(screen, (0,0,0), rect, 1)

    def set_color(self, color):
        self.color = color
        if self.imageClean == None:
            return
        self.image = self.imageClean.copy()
        self.image.fill(color, None, pygame.BLEND_RGBA_MULT)

    # Image that appears over the button
    def draw_image(self, screen):
        if self.image == None:
            return
        rect = self.posRect
        image = self.image
        if self.circular:
            screen.blit(image, (rect.x-image.get_width()/2, rect.y-image.get_height()/2))
        else:
            screen.blit(image, (rect.centerx-image.get_width()/2, rect.centery-image.get_height()/2))

class FloatingText:
    def __init__(self, textSurface, position=None):
        self.pos = position
        self.textSurface = textSurface

    def draw(self, screen):
        if self.pos == None or self.textSurface == None:
            return
        screen.blit(self.textSurface, self.pos)

def draw_title_box(screen, mdInfo, text):
    box = mdInfo.boundingBox.copy()
    rect = box.copy()
    rect.width *= 0.75
    rect.height = 128
    rect.x = box.centerx - rect.width/2
    rect.y = 28
    pygame.draw.rect(screen, (255,255,255), rect, 0)
    pygame.draw.rect(screen, (0,0,0), rect, 2)
    
    image = mdInfo.titleFont.render(text, True, (0,0,200))
    screen.blit(image, (rect.centerx-image.get_width()/2, rect.centery-image.get_height()/2))

def set_buttons_positions_0(mdInfo):
    buttons = mdInfo.allButtons[0]
    box = mdInfo.boundingBox
    # Main button size (the big buttons)
    mbs = 256
    # Single player button
    buttons[0].posRect = pygame.Rect(box.centerx-mbs/1.5, box.centery, mbs, mbs)
    # MB button
    buttons[1].posRect = pygame.Rect(box.centerx+mbs/1.5, box.centery, mbs, mbs)
    # Graphbuilder button
    buttonWidth = 160
    buttons[2].posRect = pygame.Rect(32, box.bottom-buttonWidth-32, buttonWidth, buttonWidth)

# Choose opponent
def set_buttons_positions_1(mdInfo):
    buttons = mdInfo.allButtons[1]
    box = mdInfo.boundingBox
    # Main button size (the big buttons)
    mbs = 256
    buttons[0].posRect = pygame.Rect(box.centerx-mbs/2-mbs/1.5, box.centery-mbs/2, mbs, mbs)
    buttons[1].posRect = pygame.Rect(box.centerx-mbs/2+mbs/1.5, box.centery-mbs/2, mbs, mbs)
    position_back_button(mdInfo)
    rect = buttons[1].posRect.copy()
    mdInfo.opponentLevelText.pos = (rect.x, rect.bottom + 16)
    gap=8
    rect.x -= gap/2
    rect.y = rect.bottom + 16 + mdInfo.opponentLevelText.textSurface.get_height() + 16
    rect.width = (rect.width-gap)/3
    rect.height /= 3
    buttons[2].posRect = rect
    rect = rect.copy()
    rect.x += rect.width + gap
    buttons[3].posRect = rect
    rect = rect.copy()
    rect.x += rect.width + gap
    buttons[4].posRect = rect

# Choose graph set
def set_buttons_positions_2(mdInfo):
    box = mdInfo.boundingBox
    buttons = mdInfo.allButtons[2]
    # Graph selection button
    bWidth = 256
    buttons[0].posRect = pygame.Rect(box.centerx-bWidth/2, box.centery-bWidth/2, bWidth, bWidth)
    # Prev/next buttons
    bWidth = 128
    bHeight = 128
    r = buttons[0].posRect.copy()
    buttons[1].posRect = pygame.Rect(r.x-32-bWidth, r.centery-bHeight/2, bWidth, bHeight)
    buttons[2].posRect = pygame.Rect(r.right+32, r.centery-bHeight/2, bWidth, bHeight)
    position_back_button(mdInfo)
    # Custom graph button
    buttonWidth = 160
    buttons[3].posRect = pygame.Rect(32, box.bottom-buttonWidth-32, buttonWidth, buttonWidth)

# Choose graph
def set_buttons_positions_3(mdInfo):
    box = mdInfo.boundingBox
    buttons = mdInfo.allButtons[3]
    # Graph selection button
    bWidth = 256
    buttons[0].posRect = pygame.Rect(box.centerx-bWidth/2, box.centery-bWidth/2, bWidth, bWidth)
    r = buttons[0].posRect
    mdInfo.graphFlagButton.posRect = pygame.Rect(r.right-48,r.bottom-48, 64, 64)
    # Prev/next buttons
    bWidth = 128
    bHeight = 128
    r = buttons[0].posRect.copy()
    buttons[1].posRect = pygame.Rect(r.x-32-bWidth, r.centery-bHeight/2, bWidth, bHeight)
    buttons[2].posRect = pygame.Rect(r.right+32, r.centery-bHeight/2, bWidth, bHeight)
    position_back_button(mdInfo)

def position_back_button(mdInfo):
    box = mdInfo.boundingBox
    button = mdInfo.backButton
    bSize = (128, 64)
    padding = 32
    button.posRect = pygame.Rect(box.right-32-bSize[0], box.bottom-padding-bSize[1], bSize[0], bSize[1])

# Functions that escape the menu
def goto_graph_mode(mdInfo, modeNumber=None):
    # Return if it's the null graph
    if modeNumber != 4 and mdInfo.gotoMode != 4 and mdInfo.currentGraphSet != None:
        mdInfo.selectedGraph = mdInfo.currentGraphSet[mdInfo.itemIndex[3]]
        if mdInfo.selectedGraph["graphNumber"] == -1:
            return
    if modeNumber != None:
        mdInfo.gotoMode = modeNumber
    else:
        mdInfo.gotoMode = mdInfo.nextMode

def create_tiny_graph_surfaces(graphs, surfaceSize=(256,256)):
    images = []
    for g in graphs:
        images.append(create_tiny_graph_surface(g, pygame.Surface(surfaceSize, pygame.SRCALPHA)))
    return images

# Little depictions of the graphs to show on the menu buttons
def create_tiny_graph_surface(graph, surface):
    padding = 64
    size = (surface.get_width()-padding, surface.get_height()-padding)

    # Centre the vertices in the surface
    utility.move_vertices_to_point(graph.vs, size[0]/2, size[1]/2)

    # Copy so you don't affect the actual vertices
    graph = graph.copy()
    # Shrink the graph until it fits
    for v in graph.vs:
        vertexVisible = False
        while not vertexVisible:
            if v["x"] < 0 or v["x"] > size[0] or v["y"] < 0 or v["y"] > size[1]:
                utility.converge_graph(graph.vs, 0.05, (size[0]/2, size[1]/2))
            else:
                vertexVisible = True

    # Draw edges
    for e in graph.es:
        a = e.tuple[0]
        b = e.tuple[1]
        a_pos = (int(graph.vs[a]["x"]+padding/2), int(graph.vs[a]["y"]+padding/2))
        b_pos = (int(graph.vs[b]["x"]+padding/2), int(graph.vs[b]["y"]+padding/2))
        pygame.draw.line(surface, (0,0,0), a_pos, b_pos, 1)

    # Draw vertices
    radius = 6
    if graph["graphNumber"] == -1:
        radius = 2
    for v in graph.vs:
        pygame.gfxdraw.filled_circle(surface, int(v["x"]+padding/2), int(v["y"]+padding/2), radius, (255,255,255))
        pygame.gfxdraw.aacircle(surface, int(v["x"]+padding/2), int(v["y"]+padding/2), radius, (0,0,0))

    return surface

class Menu_UserInput:
    def __init__(self, mdInfo):
        self.mdInfo = mdInfo
        self.child = None

    def mouse_down(self, mousePos, button):
        if button > 3:
            return
        if self.child is None:
            for button in self.mdInfo.allButtons[self.mdInfo.menuPage]:
                if self.check_click(mousePos, button):
                    break
        # Check bubble button
        if self.child is None:
            self.check_click(mousePos, self.mdInfo.toggleBubblesButton)
               
    def check_click(self, mousePos, button):
        if button.posRect == None:
            return False
        if type(button) is MenuButton and button.circular:
            if utility.point_is_within_circle(mousePos, (button.posRect.x, button.posRect.y), button.posRect.width/2):
                self.child = button
                return True
        elif button.posRect.collidepoint(mousePos):
            self.child = button
            return True
        return False
                
    def mouse_up(self, mousePos, button):
        if button == 3:
            prev_menu(self.mdInfo)
        elif self.child is not None:
            if type(self.child) is MenuButton and self.child.circular:
                if utility.point_is_within_circle(mousePos, (self.child.posRect.x, self.child.posRect.y), self.child.posRect.width/2):
                    self.child.pressed()
            elif self.child.posRect.collidepoint(mousePos):
                self.child.pressed()
        self.child = None
